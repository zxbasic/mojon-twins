Formato de pantalla
===================

Me gustó mucho el rolláquel que me monté en PC hace eones para pintar las pantallas basadas en caracteres coloreados, bloques 16x16 coloreados, rectángulos con textura simple y rectángulos con textura meta 2x2 (para los enladrillados).

Voy a divagar un rato para describir un formato mínimal que pueda ser descrito en una cadena que se parsée y se pinte.

Veamos: las coordenadas son siempre números que pueden ocupar hasta 5 bits, así que tengo que ver como empaquetar de forma eficiente todo. Voy a repasar el formato original del PHENG de PC para ver qué puedo sacar en claro. Obviamente, al ser para PC, será jauja en cuanto a aprovechamiento del espacio, pero bueno. Que me apetece verlo, jodoño.

Vale, ahora recuerdo. Todo se hacía por scripts, así que las pantallas se generaban como scripts, ristras de comandos en ascii. No me sirve de mucho, yo necesito un binario pequeño.

Attr
----

00000000 AAAAAAAA

Básico
------

- PAPER P
- INK I
- BRIGHT ON/OFF.

Bright indica en el motor si el bloque colisiona o no, así que es importante. Puedo usar un byte para cada uno. Puedo hacer opcodes de 4 bits:

1 PAPER
2 INK 
3 BRIGHT.

Un comando mínimo será XXXXYYYY con XXXX el opcode e YYYY el parámetro. Pero no sé si voy a implementar esto...

Pintar un carácter
------------------

PUTCHAR x, y, c

c será un carácter de los 96. x e y son coordenadas de caracter, por lo que necesitan 5 bits. En total necesito 4 + 5 + 5 + 7 bits, o 21 bits, con lo que puedo usar solo 3 bytes:

Opcode 4, "PUTCHAR"
|
0100 XXXX x··yYYYY ·CCCCCCC

La coordenada X sería XXXX + (16 si x == 1). La coordenada Y sería el segundo byte &31. 

Pintar un rectángulo de caracteres
----------------------------------

RECTCHAR x1,y1,x2,y2,c

Opcode 5, "RECTCHAR"
|
0101 XXXX xYYYYYXX xxxYYYYY ·CCCCCCC

4 bytes.

Pinta un bloque 16x16 (meta) de chars contiguos
-----------------------------------------------

BLOCK x,y,c1

Opcode 6, "BLOCK"
|
0110 XXXX x··yYYYY ·CCCCCCC

Pinta un rectángulo de bloques (meta) de chars contiguos
--------------------------------------------------------

RECTBLOCK x1,y1,x2,y2,c

Opcode 7, "RECTBLOCK"
|
0111 XXXX xYYYYYXX xxxYYYYY ·CCCCCCC

Pinta un bloque 16x16 meta, mapeado
-----------------------------------

META x,y,m

Opcode 8, "META"
|
1000 XXXX x··yYYYY ·CCCCCCC

Los metatiles van definidos aparte e indexados.

Pinta un rectángulo de bloques (meta) de chars mapeados
-------------------------------------------------------

RECTMETA x1,y1,x2,y2,m

Opcode 9, "RECTMETA"
|
1001 XXXX xYYYYYXX xxxYYYYY ·MMMMMMM



~~

El Editor
=========

El editor (en freebasic) necesitará un .bin con los 96 caracteres y un .meta (texto) con las definiciones de los metatiles. El primer metatile será el número 1. Un metatile serán 4 valores separados por comas.

~~

Exportar gráficos de tal y cual

Phantomas V
Valores charset: $58, $FB (88 , 251) -> $FB58=64344 + $100 = $FC58 (64600)
 