' crscr v0.1.20171002
' Creates block-based screens. See pheng's drafs.md for file formats.

#include "file.bi"
#include "fbpng.bi"
#include "fbgfx.bi"
#include once "crt.bi"

#include "cmdlineparser.bi"
#include "mtparser.bi"
#include "gui.bi"
#include "keycodes.bi"
#include "mtfuncs.bi"

#define RGBA_R( c ) ( CUInt( c ) Shr 16 And 255 )
#define RGBA_G( c ) ( CUInt( c ) Shr  8 And 255 )
#define RGBA_B( c ) ( CUInt( c )        And 255 )
#define RGBA_A( c ) ( CUInt( c ) Shr 24         )

#define MODE_CHAR 0
#define MODE_META 1
#define MODE_BLOCK 2

#define ST_IDLE 0 
#define ST_DRAG 1

Type PaintCommandType
	opcode as uByte
	x1 as uByte
	y1 as uByte
	x2 as uByte
	y2 as uByte
	c as uByte
End Type

Dim Shared As PaintCommandType paintCommands (255)
Dim Shared As Integer paintCommandsIndex

Dim Shared As Integer speccyColours (15) = { _
	&HFF000000, _
	&HFF0000C0, _
	&HFFC00000, _
	&HFFC000C0, _
	&HFF00C000, _
	&HFF00C0C0, _
	&HFFC0C000, _
	&HFFC0C0C0, _
	&HFF000000, _
	&HFF0000FF, _
	&HFFFF0000, _
	&HFFFF00FF, _
	&HFF00FF00, _
	&HFF00FFFF, _
	&HFFFFFF00, _
	&HFFFFFFFF _
}

Dim Shared As uByte chars (767)
Dim Shared As uByte meta (255, 3)
Dim Shared As Integer maxMeta
Dim Shared As uByte curAttr
Dim Shared As Integer curMode, curChar, curMeta
Dim Shared As Integer signalExit
Dim Shared As Integer mx, my, mbtn, pbtn

Dim Shared As Any Ptr blackBoard, backBuffer
Dim Shared As Any Ptr buttonsBoard

Const As Integer black = RGB (0,0,0)

Sub usage
	Puts "$ crscr.exe chars=chars.bin meta=metas.txt file=screen.crscr"
	Puts ""
	Puts "screen.crscr will be created if it does not exist, of course."
	Puts "This is not very user friendly, because:"
	Puts "- chars.bin must be 768 bytes (96 8x8 1-bit chars inside)."
	Puts "- metas.txt must have 4 comma-separated values per line."
	Puts "And must means MUST. [MUST]. ***MUST***, as in you won't get an error,"
	Puts "but likely a crash."
End Sub

Sub breakAttr (ByVal c As uByte, ByRef c1 As uByte, ByRef c2 As uByte)
	c1 = (c And 7): If (c And 64) = 64 Then c1 = c1 + 8
	c2 = ((c Shr 3) And 7): If (c And 64) = 64 Then c2 = c2 + 8
End Sub

Sub readChars (fName As String)
	Dim As Integer fIn
	fIn = FreeFile
	Open fName For Binary As #fIn
	Dim As Integer i
	For i = 0 To 767
		Get #fIn, , chars (i)
	Next i
	Close #fIn
End Sub

Sub readMeta (fName As String)
	Dim As Integer i
	Dim As Integer fIn
	fIn = FreeFile
	Open fName For Input As #fIn
	maxMeta = 0
	While Not Eof (fIn)
		For i = 0 To 3
			Input #1, meta (maxMeta, i)
		Next i
		maxMeta = maxMeta + 1
	Wend
	Close #fIn
End Sub

Sub loadMyStuff (fName As String)
	Dim As Integer fIn
	Dim As uByte d
	fIn = FreeFile
	Open fName For Binary As #fIn
	paintCommandsIndex = 0
	Do
		Get #fIn, , d
		If d = &HFF Then Exit Do
		paintCommands (paintCommandsIndex).opcode = d
		Get #fIn, , paintCommands (paintCommandsIndex).x1
		Get #fIn, , paintCommands (paintCommandsIndex).y1
		Get #fIn, , paintCommands (paintCommandsIndex).x2
		Get #fIn, , paintCommands (paintCommandsIndex).y2
		Get #fIn, , paintCommands (paintCommandsIndex).c
		paintCommandsIndex = paintCommandsIndex + 1
	Loop	
	Close #fIn
End Sub

Sub saveMyStuff (fName As String)
	Dim As Integer i
	Dim As Integer fOut
	Dim As uByte d
	fOut = FreeFile
	Open fName For Binary As #fOut
	For i = 0 To paintCommandsIndex - 1
		Put #fOut, , paintCommands (i).opcode
		Put #fOut, , paintCommands (i).x1
		Put #fOut, , paintCommands (i).y1
		Put #fOut, , paintCommands (i).x2
		Put #fOut, , paintCommands (i).y2
		Put #fOut, , paintCommands (i).c
	Next i
	d = &HFF: Put #fOut, , d
	Close #fOut
End Sub

Sub paintChar (img As Any Ptr, x0 As Integer, y0 As Integer, c As uByte, attr As uByte)
	Dim As Integer x, y, idx
	Dim As uByte c1, c2
	Dim As uByte l
	breakAttr attr, c1, c2
	idx = c Shl 3
	For y = 0 To 7
		l = chars (idx): idx = idx + 1
		For x = 0 To 7
			If img = 0 Then
				If l And (1 Shl (7 - x)) Then
					Line (x0 + x + x, y0 + y + y) - (x0 + x + x + 1, y0 + y + y + 1), speccyColours (c1), b
				Else
					Line (x0 + x + x, y0 + y + y) - (x0 + x + x + 1, y0 + y + y + 1), speccyColours (c2), b
				End If
			Else
				If l And (1 Shl (7 - x)) Then
					Line img, (x0 + x + x, y0 + y + y) - (x0 + x + x + 1, y0 + y + y + 1), speccyColours (c1), b
				Else
					Line img, (x0 + x + x, y0 + y + y) - (x0 + x + x + 1, y0 + y + y + 1), speccyColours (c2), b
				End If
			End If
		Next x
	Next y
End Sub

Sub paintCharSmall (img As Any Ptr, x0 As Integer, y0 As Integer, c As uByte, attr As uByte)
	Dim As Integer x, y, idx
	Dim As uByte c1, c2
	Dim As uByte l
	breakAttr attr, c1, c2
	idx = c Shl 3
	For y = 0 To 7
		l = chars (idx): idx = idx + 1
		If img = 0 Then
			Line (x0, y0 + y)-(x0 + 7, y0 + y), speccyColours (c2)
			Line (x0, y0 + y)-(x0 + 7, y0 + y), speccyColours (c1), , Cvi<16> (Chr (l) & Chr (l))
		Else
			Line img, (x0, y0 + y)-(x0 + 7, y0 + y), speccyColours (c2)
			Line img, (x0, y0 + y)-(x0 + 7, y0 + y), speccyColours (c1), , Cvi<16> (Chr (l) & Chr (l))
		End If
	Next y
End Sub

Sub paintBlock (img As Any Ptr, x0 As Integer, y0 As Integer, c As uByte, attr as uByte)
	paintChar img, x0, y0, c, attr
	paintChar img, x0 + 16, y0, c + 1, attr
	paintChar img, x0, y0 + 16, c + 2, attr
	paintChar img, x0 + 16, y0 + 16, c + 3, attr
End Sub

Sub paintMeta (img As Any Ptr, x0 As Integer, y0 As Integer, c As uByte, attr as uByte)
	paintChar img, x0, y0, meta (c, 0), attr
	paintChar img, x0 + 16, y0, meta (c, 1), attr
	paintChar img, x0, y0 + 16, meta (c, 2), attr
	paintChar img, x0 + 16, y0 + 16, meta (c, 3), attr
End Sub

Sub paintRectangleChar (img As Any Ptr, x1 As Integer, y1 As Integer, _
	x2 As Integer, y2 As Integer, c As uByte, attr As uByte)
	Dim As Integer x, y
	For y = y1 To y2
		For x = x1 To x2 
			paintChar img, 16*x, 16*y, c, attr
		Next x
	Next y
End Sub

Sub paintRectangleMeta (img As Any Ptr, x1 As Integer, y1 As Integer, _
	x2 As Integer, y2 As Integer, ChrPattern As String, attr As uByte)
	Dim As Integer x, y, c
	For y = y1 To y2
		For x = x1 To x2 
			c = Asc (Mid (ChrPattern, 1+(((y-y1) And 1) Shl 1) + ((x-x1) And 1), 1))
			paintChar img, 16*x, 16*y, c, attr
		Next x
	Next y
End Sub

Sub execCommand (img As Any Ptr, paintCommand As PaintCommandType)
	Select Case paintCommand.opcode
		case 0:
			' attr
			curAttr = paintCommand.c
		case 1:
			' paper
		case 2:
			' ink
		case 3:
			' bright
		case 4:
			' putchar
			paintChar img, 16 * paintCommand.x1, 16 * paintCommand.y1, paintCommand.c, curAttr
		case 5:
			' rectchar
			paintRectangleChar img, _
				paintCommand.x1, paintCommand.y1, _
				paintCommand.x2, paintCommand.y2, _
				paintCommand.c, curAttr
		case 6:
			' block 
			paintBlock img, 16 * paintCommand.x1, 16 * paintCommand.y1, paintCommand.c, curAttr
		case 7:
			' rectblock
			paintRectangleMeta img, _
				paintCommand.x1, paintCommand.y1, _
				paintCommand.x2, paintCommand.y2, _
				Chr (paintCommand.c) & Chr (paintCommand.c + 1) & Chr (paintCommand.c + 2) & Chr (paintCommand.c + 3), _
				curAttr
		case 8:
			' meta
			paintMeta img, 16 * paintCommand.x1, 16 * paintCommand.y1, paintCommand.c, curAttr
		case 9:
			' rectmeta
			paintRectangleMeta img, _
				paintCommand.x1, paintCommand.y1, _
				paintCommand.x2, paintCommand.y2, _
				Chr (meta (paintCommand.c, 0)) & Chr (meta (paintCommand.c, 1)) & Chr (meta (paintCommand.c, 2)) & Chr (meta (paintCommand.c, 3)), _
				curAttr
	End Select	
End Sub

Sub repaintAll (img As Any Ptr)
	Dim As Integer i
	Line img, (0, 0) - (512, 383), black, BF
	For i = 0 To paintCommandsIndex - 1
		execCommand img, paintCommands (i)
	Next i
End Sub

Sub addCommand (newCommand As PaintCommandType)
	' Add a new command, invalidate last attr if new attr.
	If newCommand.opcode = 0 And paintCommandsIndex > 0 Then
		If paintCommands (paintCommandsIndex - 1).opcode = 0 Then
			paintCommandsIndex = paintCommandsIndex - 1
		End If
	End If
	paintCommands (paintCommandsIndex) = newCommand
	paintCommandsIndex = paintCommandsIndex + 1
End Sub

Sub undoCommand
	' Undo last command, skipping attr changes.
	Do 
		If paintCommandsIndex = 0 Then Exit Do
		paintCommandsIndex = paintCommandsIndex - 1
	Loop while paintCommands (paintCommandsIndex).opcode = 0
End Sub

Sub drawCharset
	Dim As Integer x, y, i
	Dim As Any Ptr buttoneer 
	buttoneer = ImageCreate (512, 48)
	x = 0: y = 0
	For i = 0 To 95
		paintChar buttoneer, x, y, i, curAttr
		If (curMode = MODE_CHAR And curChar = i) Or _
			(curMode = MODE_BLOCK And ( _
				i = curChar Or _
				i = ((curChar + 1) And &HFF) Or _
				i = ((curChar + 2) And &HFF) Or _
				i = ((curChar + 3) And &HFF) _
			)) Then
			Line buttoneer, (x, y) - (15 + x, 15 + y), RGB (255,0,0), B
		End If
		x = x + 16: If x = 512 Then x = 0: y = y + 16
	Next i

	Put (0, 384), buttoneer, Pset
	ImageDestroy buttoneer
End Sub

Sub drawMetaSet
	Dim As Integer x, y, i
	Dim As Any Ptr buttoneer
	buttoneer = ImageCreate (512, 48, RGB (127, 127, 127))
	x = 0: y = 0
	For i = 0 To maxMeta - 1
		paintCharSmall buttoneer, x, y, meta (i, 0), curAttr
		paintCharSmall buttoneer, x + 8, y, meta (i, 1), curAttr
		paintCharSmall buttoneer, x, y + 8, meta (i, 2), curAttr
		paintCharSmall buttoneer, x + 8, y + 8, meta (i, 3), curAttr
		If curMeta = i Then
			Line buttoneer, (x, y) - (15 + x, 15 + y), RGB (255,0,0), B
		End If
		x = x + 16: If x = 512 Then x = 0: y = y + 16
	Next i
	Put (0, 384), buttoneer, Pset
	ImageDestroy buttoneer
End Sub

Sub drawColourSet
	Dim As uByte c1, c2
	Dim As Integer i
	Dim As Any Ptr buttoneer
	breakAttr curAttr, c1, c2
	buttoneer = ImageCreate (512, 48, RGB (127, 127, 127))
	Draw String buttoneer, (0, 0), "INK", black
	Draw String buttoneer, (256, 0), "PAPER", black
	For i = 0 To 15
		Line buttoneer, (16 * i + 1, 17) - (16 * i + 14, 30), speccyColours (i), BF
		If c1 = i Then Line buttoneer, (16 * i + 0, 16) - (16 * i + 15, 31), RGB (255, 0, 0), B
		Line buttoneer, (16 * i + 257, 17) - (16 * i + 270, 30), speccyColours (i), BF
		If c2 = i Then Line buttoneer, (16 * i + 256, 16) - (16 * i + 271, 31), RGB (255, 0, 0), B
	Next i
	Put (0, 384), buttoneer, Pset
	ImageDestroy buttoneer
End Sub

Sub saveButtons
	Get (0, 432)-(511, 447), buttonsBoard
End Sub

Sub restoreButtons
	Put (0, 432), buttonsBoard, Pset
End Sub

Function attrEncode (c1 As uByte, c2 As uByte) As uByte
	Dim As uByte attr
	attr = (c1 And 7) Or ((c2 And 7) Shl 3)
	If c1 > 7 Or c2 > 7 Then attr = attr Or 64
	attrEncode = attr
End Function

Sub removeMainButtons
	Line (0, 432)-(511, 447), RGB (127, 127, 127), BF
End Sub

Function dialogYesNo (caption As String) As Integer
	Dim As Integer res 
	Dim As Integer x0, y0
	Dim As Button buttonYes
	Dim As Button buttonNo
	Dim As Any Ptr rvrtimg

	saveButtons

	While (MultiKey (1) Or Window_Event_close): Wend

	res = 0
	removeMainButtons

	x0 = 512 \ 2 - 90
	y0 = 400 \ 2 - 28

	Line (x0, y0)-(x0 + 179, y0 + 55), RGBA (127,127,127,127), BF 
	Line (x0, y0)-(x0 + 179, y0 + 55), 0, B

	Var Label_a =	Label_New	(x0 + 8, y0 + 8, 18*8, 20, caption, black, RGB (127,127,127))
	buttonYes = 	Button_New	(x0 + 8, y0 + 8 + 20, 112, 20, "Yes, Paco")
	buttonNo = 		Button_New 	(x0 + 8 + 112 + 4, y0 + 8 + 20, 48, 20, "Oops!")

	Do
		If Button_Event (buttonYes) Then	res = -1: Exit Do
		If Button_Event (buttonNo) Then 	res = 0: Exit Do
		If Window_Event_Close Then 			res = -1: Exit Do
		If MultiKey (1) Then 				res = 0: Exit Do
		If MultiKey (SC_ENTER) Then			res = -1: Exit Do
	Loop

	While (MultiKey (1) Or Window_Event_close): Wend

	restoreButtons

	Return res
End Function

Sub selectColour
	Dim As uByte c1, c2
	Dim As Button buttonExit

	drawColourSet
	breakAttr curAttr, c1, c2
	saveButtons
	removeMainButtons
	buttonExit = Button_New (16, 432, 64, 16, "Volver")

	Do 
		pbtn = mbtn
		Getmouse mx, my, , mbtn
		pbtn = (pbtn XOR mbtn) AND mbtn

		If (pbtn And 1) Then
			If my >= 400 And my <= 415 Then
				If mx < 256 Then
					' Ink
					c1 = mx \ 16
					If c1 < 8 Then 
						c2 = c2 And 7
					Else
						c2 = c2 Or 8
					End If
				Else
					' Paper
					c2 = (mx - 256) \ 16
					If c2 < 8 Then
						c1 = c1 And 7
					Else
						c1 = c1 Or 8
					End If
				End If 
				curAttr = attrEncode (c1, c2)
				drawColourSet
			End If
		End If

		If Button_Event (buttonExit) Then Exit Do
		If Window_Event_Close Then signalExit = -1: Exit Do
	Loop
	
	restoreButtons
End Sub

Sub selectMeta
	Dim As Integer ntil
	Dim As uByte c1, c2
	Dim As Button buttonExit

	drawMetaSet
	saveButtons
	removeMainButtons
	buttonExit = Button_New (16, 432, 64, 16, "Cancel")

	Do 
		pbtn = mbtn
		Getmouse mx, my, , mbtn
		pbtn = (pbtn XOR mbtn) AND mbtn

		If (pbtn And 1) Then
			If my >= 384 And my < 432 Then
				ntil = (((my - 384) \ 16) * 32) + (mx \ 16)
				
				If ntil < maxMeta Then
					curMeta = ntil
					Exit Do
				End If
			End If
		End If

		If Button_Event (buttonExit) Then Exit Do
		If Window_Event_Close Then signalExit = -1: Exit Do
	Loop

	restoreButtons
End Sub

Sub updateBlackBoard
	Dim As Integer i
	Put (0, 0), blackBoard, Pset
	For i = 0 To 511 Step 16
		If i < 384 Then Line (0, i)-(511, i), RGB (64, 64, 64)
		Line (i, 0)-(i, 383), RGB (64, 64, 64)
	Next i
End Sub

Sub drawModeInfo
	Dim As uByte c1, c2
	breakAttr curAttr, c1, c2
	Line (369, 433)-(382, 446), speccyColours (c1), BF
	Line (385, 433)-(398, 446), speccyColours (c2), BF
	Line (400, 432)-(511, 447), RGB (127, 127, 127), BF
	Draw String (400, 432), "MODE:", black
	Select Case curMode
		case MODE_CHAR:
			Draw String (440, 432), "CHAR", black
			paintChar 0, 488, 432, curChar, curAttr
		case MODE_META:
			Draw String (440, 432), "META", black
			paintCharSmall 0, 488, 432, meta (curMeta, 0), curAttr
			paintCharSmall 0, 488+8, 432, meta (curMeta, 1), curAttr
			paintCharSmall 0, 488, 432+8, meta (curMeta, 2), curAttr
			paintCharSmall 0, 488+8, 432+8, meta (curMeta, 3), curAttr
		case MODE_BLOCK:
			Draw String (440, 432), "BLOCK", black
			paintCharSmall 0, 488, 432, curChar, curAttr
			paintCharSmall 0, 488+8, 432, (curChar + 1) And &HFF, curAttr
			paintCharSmall 0, 488, 432+8, (curChar + 2) And &HFF, curAttr
			paintCharSmall 0, 488+8, 432+8, (curChar + 3) And &HFF, curAttr
	End Select
End Sub

Dim As String mandatory (2) = { "chars", "meta", "file" }
Dim As Integer state, ntil
Dim As PaintCommandType curPaintCommand
Dim As uByte lastCharIdShown, charIdShown

' Buttons
Dim As Button buttonSave
Dim As Button buttonUndo
Dim As Button buttonClear
Dim As Button buttonColour
Dim As Button buttonMeta
Dim As Button buttonToggle
Dim As Button buttonExit

sclpParseAttrs

If Not sclpCheck (mandatory ()) Then usage: End

' Load stuff
readChars (sclpGetValue ("chars"))
readMeta (sclpGetValue ("meta"))

OpenWindow 512, 448, "Mojon Twins' CrScr v0.1.20171002"

buttonSave = 	Button_New (8, 432, 48, 16, "Save")
buttonUndo = 	Button_New (8+48, 432, 48, 16, "Undo")
buttonClear = 	Button_new (8+48+48, 432, 40, 16, "Cls")
buttonColour = 	Button_new (8+48+48+40, 432, 56, 16, "Color")
buttonMeta = 	Button_new (8+48+48+40+56, 432, 48, 16, "Meta")
buttonToggle = 	Button_new (8+48+48+40+56+48, 432, 40, 16, "C/B")
buttonExit = 	Button_New (8+48+48+40+56+48+40, 432, 48, 16, "Exit")

buttonsBoard = ImageCreate (512, 16, RGB (127, 127, 127))
backBuffer = ImageCreate (512, 384, black) 
blackBoard = ImageCreate (512, 384, black)

' Edit or new
If myFileExists (sclpGetValue ("file")) Then
	loadMyStuff (sclpGetValue ("file"))
Else
	paintCommandsIndex = 0
End If

curMode = MODE_CHAR: curChar = 0: curMeta = 0
lastCharIdShown = &HFF

curAttr = 7+64

drawCharset
drawModeInfo
repaintAll blackBoard
updateBlackBoard

signalExit = 0
state = ST_IDLE

Do
	pbtn = mbtn
	Getmouse mx, my, , mbtn
	pbtn = (pbtn XOR mbtn) AND mbtn

	If my >= 384 And my < 432 And mx >= 0 And mx < 512 Then
		charIdShown = (((my - 384) \ 16) * 32) + (mx \ 16)
		If charIdShown <> lastCharIdShown Then
			Line (344, 432)-(367, 447), RGB (127, 127, 127), BF
			Draw String (344, 432), Trim (Str (charIdShown)), black 
			lastCharIdShown = charIdShown
		End If
	End If

	If pbtn And 1 Then
		If my >= 384 And my < 432 Then
			curChar = (((my - 384) \ 16) * 32) + (mx \ 16)
			If curMode = MODE_META Then curMode = MODE_CHAR
			drawCharset
			drawModeInfo
		End If
	End If

	If my >= 0 And my < 384 And mx >= 0 And mx < 512 Then
		If mbtn And 1 Then
			If state = ST_IDLE Then
				state = ST_DRAG
				curPaintCommand.x1 = mx\16
				curPaintCommand.y1 = my\16
				Get (0, 0)-(511,383), backBuffer
			Else
				Put (0, 0), backBuffer, Pset
				curPaintCommand.x2 = mx\16
				curPaintCommand.y2 = my\16
				Line (curPaintCommand.x1*16, curPaintCommand.y1*16)-(curPaintCommand.x2*16 + 15, curPaintCommand.y2*16 + 15), RGB (255,0,0), B
			End If
		Else
			If state = ST_DRAG Then
				' Write command type
				Select Case curMode
					case MODE_CHAR:
						If curPaintCommand.x1 = curPaintCommand.x2 And _
							curPaintCommand.y1 = curPaintCommand.y2 Then
							curPaintCommand.opcode = 4	' PUTCHAR
						Else
							curPaintCommand.opcode = 5	' RECTCHAR
						End If
						curPaintCommand.c = curChar
					case MODE_BLOCK
						If curPaintCommand.x1 = curPaintCommand.x2 And _
							curPaintCommand.y1 = curPaintCommand.y2 Then
							curPaintCommand.opcode = 6	' PUTBLOCK
						Else
							curPaintCommand.opcode = 7 ' RECTBLOCK
						End If
						curPaintCommand.c = curChar
					case MODE_META
						If curPaintCommand.x1 = curPaintCommand.x2 And _
							curPaintCommand.y1 = curPaintCommand.y2 Then
							curPaintCommand.opcode = 8	' PUTMETA
						Else
							curPaintCommand.opcode = 9 	' RECTMETA
						End If
						curPaintCommand.c = curMeta
				End Select
				execCommand blackBoard, curPaintCommand
				updateBlackBoard
				addCommand curPaintCommand
				state = ST_IDLE
			End If
		End If
	End If		

	If Button_Event (buttonSave) Then
		saveMyStuff sclpGetValue ("file")
		repaintAll blackBoard
		updateBlackBoard
		drawModeInfo
	End If

	If Button_Event (buttonUndo) Then
		undoCommand
		repaintAll blackBoard
		updateBlackBoard
		If paintCommandsIndex > 0 Then
			If paintCommands (paintCommandsIndex - 1).opcode = 0 Then
				curAttr = paintCommands (paintCommandsIndex - 1).c
			End If
		End If		
		drawModeInfo
	End If

	If Button_Event (buttonClear) Then
		If dialogYesNo ("Are you sure?") Then
			paintCommandsIndex = 0
		End If
		repaintAll blackBoard
		updateBlackBoard
		drawModeInfo
	End If

	If Button_Event (buttonColour) Then
		selectColour
		curPaintCommand.c = curAttr
		curPaintCommand.opcode = 0	' ATTR
		addCommand curPaintCommand
		drawCharset
		drawModeInfo
	End If

	If Button_Event (buttonMeta) Then
		curMode = MODE_META
		selectMeta
		drawCharset
		drawModeInfo
	End If

	If Button_Event (buttonToggle) Then
		If curMode = MODE_CHAR Then
			curMode = MODE_BLOCK
		Else
			curMode = MODE_CHAR
		End If
		drawCharset
		drawModeInfo
	End If

	If signalExit Or MultiKey (1) Or Button_Event (buttonExit) Or Window_Event_Close Then
		If dialogYesNo ("Exit program?") Then Exit Do 
		signalExit = 0
	End If	
Loop

ImageDestroy buttonsBoard
ImageDestroy blackBoard
ImageDestroy backBuffer
