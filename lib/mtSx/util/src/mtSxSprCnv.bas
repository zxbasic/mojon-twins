' mtsxsprcnv.bas v0.1.20170929

#include "file.bi"
#include "fbpng.bi"
#include "fbgfx.bi"
#include once "crt.bi"

#include "cmdlineparser.bi"
#include "mtparser.bi"

#define RGBA_R( c ) ( CUInt( c ) Shr 16 And 255 )
#define RGBA_G( c ) ( CUInt( c ) Shr  8 And 255 )
#define RGBA_B( c ) ( CUInt( c )        And 255 )
#define RGBA_A( c ) ( CUInt( c ) Shr 24         )

Const As Integer black = RGB (0,0,0)

Sub usage
	Puts "$ mtSxSprCnv.exe in=file.png out=file.bas label=label [max=n]"
End Sub

Function readBitmapFrom (x As Integer, y As Integer, img As Any Ptr) As uByte
	Dim As uByte res
	Dim As Integer i

	res = 0
	For i = 0 To 7
		If Point (x + i, y, img) <> black Then res = res Or (1 Shl (7-i))
	Next i

	readBitmapFrom = res
End Function

Dim As String mandatory (2) => { "in", "out", "label" }
Dim As Any Ptr img
Dim As Integer wIn, hIn, fOut, max, ct
Dim As Integer ws, hs
Dim As Integer x, y, xx, yy

Puts "mtSxSprCnv v0.1.20170929"

sclpParseAttrs

If Not sclpCheck (mandatory ()) Then usage: End

screenres 640,480, 32, , -1

img = png_load ( sclpGetValue ("in") )
If ImageInfo (img, wIn, hIn, , , , ) Then
	Puts "There was an error reading input file. Shitty png?": End
End If

ws = wIn \ 16
hs = hIn \ 16
max = Val (sclpGetValue ("max"))
If max = 0 Then max = ws * hs

fOut = FreeFile
Open sclpGetValue ("out") For Output As #fOut

Print #fOut, "' Converted by mtSxSprCnv.exe v0.1.20170929"
Print #fOut, "' Copyleft 2012-2017 by The Mojon Twins"
Print #fOut, ""
Print #fOut, "' Contains " & max & " sprite faces, 32 bytes each."
Print #fOut, ""
Print #fOut, "Sub gfxSetContainer" & sclpGetValue ("label")
Print #fOut, "    gfxSet" & sclpGetValue ("label") & ":"
Print #fOut, "    Asm"
Print #fOut, "        gfx_set_" & sclpGetValue ("label") & ":"

ct = 0

For yy = 0 To hs - 1
	For xx = 0 To ws - 1
		If ct < max Then
			Print #fOut, "        ; Sprite face #" & ct & ", cut from (" & (yy*16) & ", " & (xx*16) & ")."
			For y = 0 To 15
				Print #fOut, "            defb ";
				Print #fOut, readBitmapFrom (xx*16, yy*16 + y, img) & ", ";
				Print #fOut, readBitmapFrom (xx*16 + 8, yy*16 + y, img)
			Next y		
			ct = ct + 1
		End If
	Next xx
Next yy

Print #fOut, "    End Asm"
Print #fOut, "End Sub"
Print #fOut, ""

Puts "Done thing. " & ct & " sprites extracted."
Puts "BASIC label: gfxSet" & sclpGetValue ("label") & "; ASM label: gfx_set_" & sclpGetValue ("label")
Puts "Sprite face N is at (@gfxSet" & sclpGetValue ("label") & " + (n Shl 5))"
