' mtSx simple example

#include once "sprites.bas"
#include once "mtsx.bas"

Dim i As uByte
Dim sx (3) As uByte
Dim sy (3) As uByte
Dim smx (3) As Byte 
Dim smy (3) As Byte

mtSxInit ()
Border 7: Paper 7: Ink 0: Cls

Print At 0, 0; "Simple XOR blit text (no sprites)"

For i = 0 To 15
	mtSxPutXorSprite (i Shl 4, 8, @gfxSetTest + (i Shl 5))
Next i

Print At 4, 0; "And 4 sprites moving..."

For i = 0 To 3
	sx (i) = Int (Rnd * 240)
	sy (i) = Int (Rnd * 176)
	smx (i) = (Int (Rnd * 1) Shl 1) - 1
	smy (i) = (Int (Rnd * 1) Shl 1) - 1

	mtSxActivateSpr (i)
	mtSxSetFrameSpr (i, @gfxSetTest + (i Shl 5))
Next i

For i = 4 To 7
	mtSxDeactivateSpr (i)
Next i

While Inkey$ = ""
	For i = 0 To 3
		sx (i) = sx (i) + smx (i)
		sy (i) = sy (i) + smy (i)
		
		If sx (i) = 0 Or sx (i) = 239 Then smx (i) = -smx (i): End If
		If sy (i) = 0 Or sy (i) = 175 Then smy (i) = -smy (i): End If
		mtSxMoveSpr (i, sx (i), sy (i))
	Next i

	mtSxUpdateSprites ()
	Asm
		ld a, 0
		out (254),a
	End Asm
Wend
