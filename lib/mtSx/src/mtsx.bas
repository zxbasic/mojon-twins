' Contains code from  http://chuntey.wordpress.com/2010/03/22/sprite-graphics-tutorial/
' I modified this myself. I hope it works for 16x16 XOR sprites and is useable.

' A bit slow... Will suffice? :-S

Dim mtSprBank as uInteger

Sub mtSxInit
	' Builds the tables we need.
	' 65024 (MSB) and 65280 (LSB)
	Asm
	
		ld de,16384
		ld hl,65024
		ld b,192
	
	lineloop:
		ld (hl),e
		inc h
		ld (hl),d
		dec h
		inc l
	
		inc d
		ld a,d
		and 7
		jr nz,nextline
		ld a,e
		add a,32
		ld e,a
		jr c,nextline
		ld a,d
		sub 8
		ld d,a
	
	nextline:
		djnz lineloop
		
	End Asm
End Sub

Sub mtSxPutXorSprite (x as uByte, y as uByte, address as uInteger)
	Poke 1 + @mtSxXorSpriteVars, x
	Poke @mtSxXorSpriteVars, y
	Poke uInteger 1 + @mtSxXorSpritePunchAddress, address
	Asm
		Call mtSx_XorSprite
	End Asm
End Sub

Sub mtSxActivateSpr (u as uByte)
	mtSprBank = (u << 4) + @mtSxSpriteDescriptors
	Poke 9 + mtSprBank, 1
End Sub

Sub mtSxDeactivateSpr (u as uByte)
	mtSprBank = (u << 4) + @mtSxSpriteDescriptors
	Poke mtSprBank, 0
End Sub

Sub mtSxSetFrameSpr (u as uByte, ptr as uInteger)
	mtSprBank = (u << 4) + @mtSxSpriteDescriptors
	Poke uInteger 5 + mtSprBank, ptr
End Sub

Sub mtSxMoveSpr (u as uByte, x as uByte, y as uByte)
	mtSprBank = (u << 4) + @mtSxSpriteDescriptors
	Poke 2 + mtSprBank, x
	Poke 1 + mtSprBank, y
End Sub

Sub mtSxUpdateSprites ()
	Asm
		push ix
		Call mtSx_UpdateSprites
		pop ix
	End Asm
End Sub

Sub mtSxCodeContainer
mtSxPutTile16:
	Asm
	mtSxPutTile16:
		ld bc, (mtsx_y)
		ld	h, 254		; high byte of start of table
		ld	l, c		; C reg. contains the Y coord
		ld	a, (hl)		; already we have found the low byte, so store in A temporarily
		inc h			; increasing high byte moves forward 256 bytes, and to the
		ld	h, (hl)		; corresponding high byte of the screen address		
		ld	l, a		; HL now contains the screen address for the start of the line
		
		ld 	a, b		; calculate character column 0 - 31 from X coord
		and $f8			; isolate appropriate bits
		rrca			; rotate right circular accumulator (faster than srl a)
		rrca
		rrca			; shifting three times is the same as dividing by eight
		add	a, l		; add to low byte of address
		ld	l, a
	
		; Save this for later
		
		ld (mtsx_scradd), hl
	End Asm
mtSxPutTile16PunchAddress:
	Asm
	mtSxPutTile16PunchAddress:
		ld	hl, mtSx_SpriteData
		; 16 lines 
		ld	c, 16
		
	mtSx_PTlineLoop:
		
		; load regs with sprite data
		ld	d, (hl)
		inc	hl
		ld	e, (hl)
		inc hl
		push hl
		
		ld	hl, (mtsx_scradd)
		
		ld	(hl), d
		inc	l
		ld	(hl), e
		
		dec l
		inc h
		
		; Adjust for next char/segment boundary
		
		ld	a, h
		and	7
		jr	nz, mtSx_A2
		ld	a, l
		add	a, 32
		ld	l, a
		jr	c, mtSx_A2
		ld	a, h
		sub	8
		ld	h, a
		
	mtSx_A2:
		ld	(mtsx_scradd), hl
		pop	hl
		
		dec	c
		
		; next n
		jp	nz, mtSx_PTlineLoop

		ret		
	End Asm		

mtSxXorSprite:
	Asm
	mtSx_XorSprite:
	
		ld bc, (mtsx_y)		; Now B = X, C = Y
		
		; Get screen address from table
		
		ld	h, 254		; high byte of start of table
		ld	l, c		; C reg. contains the Y coord
		ld	a, (hl)		; already we have found the low byte, so store in A temporarily
		inc h			; increasing high byte moves forward 256 bytes, and to the
		ld	h, (hl)		; corresponding high byte of the screen address		
		ld	l, a		; HL now contains the screen address for the start of the line
		
		ld 	a, b		; calculate character column 0 - 31 from X coord
		and $f8			; isolate appropriate bits
		rrca			; rotate right circular accumulator (faster than srl a)
		rrca
		rrca			; shifting three times is the same as dividing by eight
		add	a, l		; add to low byte of address
		ld	l, a
	
		; Save this for later
		
		ld (mtsx_scradd), hl
		
		ld	a, b
		and	7

		; Here, calculate destination into rotate code block

		; a = 7 - a
		neg
		add	a, 7		

		ld 	b, a 
		add a, a
		add a, a
		add a, b 

		ld  hl, mtSx_rotateBlock
		ld 	b, 0
		ld 	c, a 
		add hl, bc

		ld  (1 + mtSx_punchBlockAddress), hl
		
	End Asm
' Use this label to punch the sprite address onto the code with a poke uInteger
mtSxXorSpritePunchAddress:
	Asm
	mtSx_XorSpritePunchAddress:
		ld	hl, mtSx_SpriteData
		
		; 16 lines 
		ld	c, 16
		
	mtSx_lineLoop:
		
		; load regs with sprite data
		ld	d, (hl)
		inc	hl
		ld	e, (hl)
		inc hl
		push hl

; The jump address is calculated just once and then
; punched onto the jp $0000 below:

		xor a

mtSx_punchBlockAddress:
		jp $0000
		
mtSx_rotateBlock:

		rr d 	; 2 bytes
		rr e 	; 2 bytes 
		rra 	; 1 byte

		rr d 	; 2 bytes
		rr e 	; 2 bytes 
		rra 	; 1 byte

		rr d 	; 2 bytes
		rr e 	; 2 bytes 
		rra 	; 1 byte
		
		rr d 	; 2 bytes
		rr e 	; 2 bytes 
		rra 	; 1 byte

		rr d 	; 2 bytes
		rr e 	; 2 bytes 
		rra 	; 1 byte
		
		rr d 	; 2 bytes
		rr e 	; 2 bytes 
		rra 	; 1 byte

		rr d 	; 2 bytes
		rr e 	; 2 bytes 
		rra 	; 1 byte		

	mtSx_skipRotate:
		ld	b, a
		ld	hl, (mtsx_scradd)
		
		; Xor with screen
		
		ld	a, (hl)
		xor	d
		ld	(hl), a
		inc	l
		ld	a, (hl)
		xor e
		ld	(hl), a
		inc l
		ld	a, (hl)
		xor b
		ld	(hl), a
		
		; Next line
		
		dec l
		dec l
		inc h
		
		; Adjust for next char/segment boundary
		
		ld	a, h
		and	7
		jr	nz, mtSx_A1
		ld	a, l
		add	a, 32
		ld	l, a
		jr	c, mtSx_A1
		ld	a, h
		sub	8
		ld	h, a
		
	mtSx_A1:
		ld	(mtsx_scradd), hl
		pop	hl
		
		dec	c
		
		; next n
		jp	nz, mtSx_lineLoop

		ret		
	End Asm

mtSxXorSpriteVars:
	Asm
	mtSx_XorSpriteVars:
	mtsx_y: defb 0
	mtsx_x: defb 0
	mtsx_scradd: defw 0
	mtsx_bitpos: defb 0
	End Asm
	
	Asm
	mtSx_UpdateSprites:
		; For each sprite in the sprite descriptor table
		; If active XOR oldframe @cx,cy
		; If new OR active XOR newframe @x,y
		; new = 0
		; cx = x; cy = y;
		; oldframe = newframe
		halt
		ld	ix, mtSx_SpriteDescriptors
		ld	b, 0
		ld	c, 8
		
	mtSx_updateLoop:
		push	bc
		
		; If active XOR oldframe @cx, cy
		ld	a, (ix+0)
		or	a
		jr	z, mtSx_uL1
	
		ld	a, (ix+3)
		ld	(mtsx_y), a
		ld 	a, (ix+4)
		ld	(mtsx_x), a
		ld	a, (ix+7)
		ld	(1+mtSx_XorSpritePunchAddress), a
		ld	a, (ix+8)
		ld	(2+mtSx_XorSpritePunchAddress), a
		
		call mtSx_XorSprite
	mtSx_uL1:

		; If new OR active XOR newframe @cx, cy
		ld	a, (ix+0)
		ld	b, a
		ld	a, (ix+9)
		or	b
		or	a
		jr	z, mtSx_nextUpdateLoop
		
		ld	a, (ix+1)
		ld	(mtsx_y), a
		ld	a, (ix+2)
		ld	(mtsx_x), a
		ld	a, (ix+5)
		ld	(1+mtSx_XorSpritePunchAddress), a
		ld	a, (ix+6)
		ld	(2+mtSx_XorSpritePunchAddress), a
		
		call mtSx_XorSprite
		
		; new = 0
		ld	(ix+9), 0
		; active = 1
		ld	(ix+0), 1

		; cx = x; cy = y
		ld	a, (ix+1)
		ld	(ix+3), a
		ld	a, (ix+2)
		ld	(ix+4), a
		
		; oldframe = newframe
		ld	a, (ix+5)
		ld	(ix+7), a
		ld	a, (ix+6)
		ld	(ix+8), a
		
	mtSx_nextUpdateLoop:
		; next descriptor
		ld	de, 16
		add	ix, de
		
		pop	bc
		dec	c
		jp nz, mtSx_updateLoop
		
		ret
	End Asm
	
' Sprite Descriptors
mtSxSpriteDescriptors:
	Asm
	mtSx_SpriteDescriptors:
		; 8 descriptors, 10 bytes each.
		; bytes are:
		; 0 active
		; 1 y
		; 2 x
		; 3 cy
		; 4 cx
		; 5,6 curframe
		; 7,8 oldframe
		; 9 new
		defs 8*16, 0
		
	mtSx_SpriteData:
		
	End Asm
End Sub
