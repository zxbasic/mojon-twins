mtSx
====

**mtSx** is a simple set of sprite routines. It's a bit on the slow side and does not support masks (just old school XOR blitting), but it gets its job done and it's easy to work with.

I created this library using lots of code I picked up elsewhere, mainly here:  http://chuntey.wordpress.com/2010/03/22/sprite-graphics-tutorial/ . I later modified the code so instead of a loop for rotating each line of each sprite, I use a table of rotations and jump in the middle of the block, depending on how many bits I need to rotate. The jump address is precalculated just once per sprite.

There's a simple test program to show the stuff in action.

Short reference
===============

First of all, **mtSx** needs some data in RAM. First thing you should do is call

```
	mtSxInit ()
```

**mtSx** supports up to 8 sprites. To activate / deactivate them (meaning they show and are taken in account or not) use:

```BASIC
	mtSxActivateSpr (n)

	mtSxDeactivateSpr (n)
```

With `n = 0..7`.

Once a sprite is activated, you can update its location (that is, move it elsewhare) using 

```BASIC
	mtSxMoveSpr (n, x, y)
```

Where `n` is the sprite number, and `x`, `y` are the new coordinates.

Sprites are assigned a sprite face, or graphic. It consists on a 16x16 pixel image described as 16 lines of 2 bytes. The small command line utility mtSxSprCnv will convert a png spriteset into a suitable .bas data file.

```BASIC
	mtSxSetFrameSpr (n, ptr)
```

Where `n` is the sprite number, and `ptr` is the address of the sprite face.

Once you have all your sprites in place, just call 

```BASIC
	mtSxUpdateSprites
```

This will erase the sprites from their old position and draw them onto the new, if they are active.

You can also draw a sprite face whenever you want. Note that this is **not** a Sprite, it's just a matters of displaying sprite faces on screen (static):

```BASIC
	mtSxPutXorSprite (x, y, ptr)
```

Where `x`, `y` are the coordinates, and `ptr` is a pointer to the sprite face. Sprite is XOR-mixed with whatever's on screen.

Converter
=========

Use the included converter to convert a spritesheet in .png format to a .bas file containing the sprite faces in the correct format as used by mtSx.

```
$ mtSxSprCnv.exe in=file.png out=file.bas label=label
```

Will use `label` in the code.
