' crscr_renderer tester
' Copyleft 2017 by The Mojon Twins

#include once "crscr_renderer.bas"

' You need:
' - a 768 byte charset (96 UDGs)
' - (optionally) a metatileset, 4 tile indexes per metatile.
' - Every screen you want to display

' You usually setup this stuff in individual source files but meh

' The 768 byte charset (96 UDGs)
' This binary contains 8 bytes per character.
Sub charsetTestContainer
    charsetTest:
    Asm
        charset_test:
            ; Include a 768 byte charset (96 UDGs)
            IncBin "udg_ross.bin"
    End Asm
End Sub

' Metatileset table. 
' Each metatile is build using 4 chars:
'     AB
'     CD 
' becomes
'     defb A, B, C, D
Sub metatileSetTestContainer
    metatileSetTest:
    Asm
        metatile_set_test:
            defb 65, 66, 67, 68
            defb 69, 70, 71, 72
            defb 73, 74, 75, 76
            defb 77, 78, 79, 80
            defb 81, 82, 83, 84
            defb 85, 86, 87, 88
            defb 89, 90, 90, 89
    End Asm
End Sub

Sub screensTestContainer
    screenTest1:
    Asm
        screen_test_1:
            IncBin "test1.crscr.bin"
    End Asm
End Sub

Dim gpit As uByte

Border 0: Paper 0: Ink 7: Cls

' First, init the library telling where our stuff is
' (that is, 768 byte charset (96 UDGs) and metatiles table)
crscrInit (@charsetTest, @metatileSetTest)

' Now call the renderer for the test screen.
crscrRender (@screenTest1)
