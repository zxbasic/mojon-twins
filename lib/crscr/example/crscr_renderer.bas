' crscr renderer for ZX Basic
' takes packed crscr screens as output by crscrcomp.exe
' Copyleft 2010-2017 by The Mojon Twins

Dim crscrMetatileAddress As uInteger
Dim crscrGpAddr As uInteger
Dim crscrMetaMatrix (3) As uByte

Sub crscrInit (charsetAddress As uInteger, metatileAddress As uInteger)
    Poke uInteger @crscrDataPool,       charsetAddress
    crscrMetatileAddress = metatileAddress
End Sub

'                  5           7           9           11
Sub crscrDrawChar (x As uByte, y As uByte, c As uByte, a As uByte)
    Asm
        ; Get screen address. Adapted from code by LCD
        ld      a, (ix+5)       ; X 
        and     $1F
        ld      l, a
        ld      a, (ix+7)       ; Y
        ld      c, a
        and     $18
        add     a, $40          ; Screen page, $40
        ld      h, a
        ld      a, c
        and     $07
        rrca
        rrca
        rrca
        or      l
        ld      l, a

        ex      de, hl          ; Screen address is in DE.

        ; Get charsetAddress
        ld      l, (ix+9)
        ld      h, 0
        add     hl, hl          ; x2 
        add     hl, hl          ; x4
        add     hl, hl          ; x8
        ld      bc, (crscr_charset_address)
        add     hl, bc

        ; Copy 8 bytes
        ld      a, (hl)
        ld      (de), a
        inc hl
        inc d 
        ld      a, (hl)
        ld      (de), a
        inc hl
        inc d 
        ld      a, (hl)
        ld      (de), a
        inc hl
        inc d 
        ld      a, (hl)
        ld      (de), a
        inc hl
        inc d 
        ld      a, (hl)
        ld      (de), a
        inc hl
        inc d 
        ld      a, (hl)
        ld      (de), a
        inc hl
        inc d 
        ld      a, (hl)
        ld      (de), a
        inc hl
        inc d 
        ld      a, (hl)
        ld      (de), a
        
        ; Get screen attr address
        ld      a, (ix+7)       ; Y
        rrca 
        rrca
        rrca                    ; x32
        ld      l, a
        and     $03
        add     a, $58          ; Attributes page, $58
        ld      h, a
        ld      a, l
        and     $E0
        ld      l, a
        ld      a, (ix+5)       ; X
        add     a, l
        ld      l, a

        ; Write attribute
        ld      a, (ix+11)
        ld      (hl), a
    End Asm
End Sub

Sub crscrDrawRectChar (x1 As uByte, y1 As uByte, x2 As uByte, y2 As uByte, c As uByte, a As uByte)
    Dim x, y As Integer
    
    For y = y1 To y2
        For x = x1 To x2
            crscrDrawChar (x, y, c, a)
        Next x
    Next y
End Sub

Sub crscrDrawRectMeta (x1 As uByte, y1 As uByte, x2 As uByte, y2 As uByte, a As uByte)
    Dim x, y As Integer

    For y = y1 To y2
        For x = x1 To x2
            crscrDrawChar (x, y, crscrMetaMatrix ((((y - y1) bAnd 1) Shl 1) bOr ((x - x1) bAnd 1)), a)
        Next x
    Next y
End Sub

Sub crscrDrawMeta (x As uByte, y As uByte, a As uByte)
    crscrDrawChar (x    , y    , crscrMetaMatrix (0), a)
    crscrDrawChar (x + 1, y    , crscrMetaMatrix (1), a)
    crscrDrawChar (x    , y + 1, crscrMetaMatrix (2), a)
    crscrDrawChar (x + 1, y + 1, crscrMetaMatrix (3), a)
End Sub

Sub crscrRender (address As uInteger)
    Dim d0, d1, d2 As uByte
    Dim opcode, x1, y1, x2, y2, c As uByte
    Dim curAttr As uByte
    
    Do 
        d0 = Peek (address): address = address + 1
        If d0 = $FF Then Exit Do: End If

        opcode = d0 Shr 4

        ' Unpack 2, 3 or 4 bytes
        If opcode = 0 Then
            c = Peek (address): address = address + 1
        ElseIf opcode = 4 Or opcode = 6 Or opcode = 8 Then
            ' d0       d1       c
            ' OOOOXXXX X··YYYYY CCCCCCCC
            d1 = Peek (address): address = address + 1
            c  = Peek (address): address = address + 1
            
            x1 = ((d0 bAnd $0F) Shl 1): If (d1 bAnd 0x80) Then x1 = x1 + 1: End If
            y1 = d1 bAnd $1F

        ElseIf opcode = 5 Or opcode = 7 Or opcode = 9 Then
            ' d0       d1       d2       c
            ' OOOOXXXX XYYYYYXX XXXYYYYY CCCCCCCC
            d1 = Peek (address): address = address + 1
            d2 = Peek (address): address = address + 1
            c = Peek (address): address = address + 1

            x1 = ((d0 bAnd $0F) Shl 1): If (d1 bAnd 0x80) Then x1 = x1 + 1: End If
            y1 = (d1 Shr 2) bAnd $1F
            x2 = ((d1 bAnd $03) Shl 3) bOr (d2 Shr 5)
            y2 = d2 bAnd $1F
        End If

        ' Execute commands
        If opcode = 0 Then
            curAttr = c
        ElseIf opcode = 4 Then
            crscrDrawChar (x1, y1, c, curAttr)
        ElseIf opcode = 5 Then
            crscrDrawRectChar (x1, y1, x2, y2, c, curAttr)
        ElseIf opcode = 6 Then
            crscrMetaMatrix (0) = c
            crscrMetaMatrix (1) = c + 1
            crscrMetaMatrix (2) = c + 2
            crscrMetaMatrix (3) = c + 3
            crscrDrawMeta (x1, y1, curAttr)
        ElseIf opcode = 7 Then
            crscrMetaMatrix (0) = c
            crscrMetaMatrix (1) = c + 1
            crscrMetaMatrix (2) = c + 2
            crscrMetaMatrix (3) = c + 3
            crscrDrawRectMeta (x1, y1, x2, y2, curAttr)
        ElseIf opcode = 8 Then
            crscrGpAddr = (Cast (uInteger, c) Shl 2) + crscrMetatileAddress
            crscrMetaMatrix (0) = Peek (crscrGpAddr)
            crscrMetaMatrix (1) = Peek (crscrGpAddr + 1)
            crscrMetaMatrix (2) = Peek (crscrGpAddr + 2)
            crscrMetaMatrix (3) = Peek (crscrGpAddr + 3)
            crscrDrawMeta (x1, y1, curAttr)
        ElseIf opcode = 9 Then
            crscrGpAddr = (Cast (uInteger, c) Shl 2) + crscrMetatileAddress
            crscrMetaMatrix (0) = Peek (crscrGpAddr)
            crscrMetaMatrix (1) = Peek (crscrGpAddr + 1)
            crscrMetaMatrix (2) = Peek (crscrGpAddr + 2)
            crscrMetaMatrix (3) = Peek (crscrGpAddr + 3)
            crscrDrawRectMeta (x1, y1, x2, y2, curAttr)
        End If
    Loop
End Sub

Sub crscrCodeContainer ()
crscrDataPool:
    Asm
        crscr_charset_address:
            defw 0
    End Asm
End Sub
