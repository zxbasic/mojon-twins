crscr
=====

**crscr** is a quick'n'dirty background renderer you can use in games. It uses a list of drawing primitives to create a background:

* **putchar** draws a single, coloured 8x8 UDG.
* **rectchar** draws a rectangle made of a single, coloured 8x8 UDG.
* **putblock** draws a 16x16 pixels block made of 4 consecutive UDGs.
* **rectblock** draws a rectangle made of such blocks.
* **putmeta** draws a 16x16 pixels block made of 4 UDGs from a table (metatile).
* **rectmeta** draws a rectangle made of such metatiles.

The *scripts* containing the list of primitives are bit-packed so they are usually small. Backgrounds are created using the supplied util crscr.exe, then packed using crscrcomp.exe, and finally included as binaries in your game and displayed using the library.

Short reference
===============

First of all, **crscr** needs to know where its stuff resides. The first thing you must do is providing the stuff it needs.

Basicly it needs a 768 bytes charset (96 UDGs) you can include via a `IncBin` statement from inline assembly (for example). Besides, if you use metatiles, it needs a list of such metatiles.

Once you have those things in place, initialize the library:

```
	crscrInit (@charSet, @metatileSet)
```

Where `charSet` and `metatileSet` are labels.

Packed backgrounds are included via `IncBin` as well. Once you have them in place, you can display them at anytime calling `crscrRender`

```
	crscrRender (@background)
```

Where `background` is a label.

Auxiliary functions
-------------------

**crscr** uses some auxiliary functions you may find useful.

```
	crscrDrawChar (x, y, c, a)
```

Draws character `c` with attribute `a`, at character coordinates `(x, y)`. It's written in assembly and it's fairly fast.

```
	crscrDrawRectChar (x1, y1, x2, y2, c, a)
```

Draws a rectangle of char `c` with attribute `a`, from `(x1,y1)` to `(x2,y2)`.

```
	crscrDrawRectMeta (x1, y1, x2, y2, a)
```

Draws a rectangle of metatiles as described in the global array `crscrMetaMatrix`. Just assign 4 UDG indexes to `crscrMetaMatrix` (0 to 3) and call the function.

```
	crscrDrawMeta (x, y, a)
```

Draws the metatile in the global array `crscrMetaMatrix` at `(x, y)` using attribute `a`.

Drawing
=======

You can use the `crscr.exe` application in the utils folder to create your screens. When you are done, you must use `crscrcomp.exe` to pack them.

`crscr.exe` takes three command line parameters:

```
$ crscr.exe chars=chars.bin meta=metas.txt file=screen.crscr
```

* `chars.bin` must be a 768 bytes file containing 96 UDGs, 8 bytes per UDG.
* `metas.txt` must be a text file containing metatile definitions. Metatiles are, as mentioned, made by 2x2 UDGs. In the text file, just add one line per metatile listing the 4 UDGs, row-ordered, comma-separated. For example:

```
65, 66, 67, 68
69, 70, 71, 72
73, 74, 75, 76
77, 78, 79, 80
81, 82, 83, 84
85, 86, 87, 88
89, 90, 90, 89
```

* `screen.crscr` is the file containing the background. If it does not exist, it will be created (empty). If it does exist, it will load it so you can edit it.

Once you run the command line, the application will open. There are several buttons in the bottom end of the window:

* **Save** will save your background to the output file (parameter `file`in the command line).
* **Undo** will undo your last action. You can undo to the begining of time. *Warning - you **can't** redo what you have undone!*.
* **Cls** will delete everything.
* **Color** will let you choose a new INK/PAPER combination.
* **Meta** will enter Metatiles mode and let you choose a metatile from the list you defined in the `meta` parameter in the command line).
* **C/B** will toggle between **Char** and **Block** modes. As mentioned, a Block is a special, automatic metatile made of 4 consecutive chars in the charset.
* **Exit** - you know.

Click on the blackboard to place a character, block or metatile (depending on the mode). Click and drag on the blackboard to create a rectangle of characters, blocks or metatiles.

Click on the charset (just beneath the blackboard) to select the current character or the first character in the current block, depending on the mode. If you are in metatiles mode and click the charset, you will enter characters mode.

There's info about the mode, character/block/metatile selected and colours active in the bottom right side of the window.

Once you have your design, exit the editor and, back in the command line, run `crscrcomp.exe`:

```
$ crscrcomp.exe in=screen.crscr out=screen.crscr.bin
```

* `screen.crscr` is your background as spat by `crscr.exe`.
* `screen.crscr.bin` is the output filename: a packed binary.

The output file is what you need to `IncBin` in your game!

The example
===========

There's an example in the **example** folder. First of all, the background image `test1.crsrc` was created:

```
$ ..\utils\crscr.exe chars=udg_ross.bin meta=meta_ross.txt file=test1.crscr
```

Once saved, it was packed:

```
$ ..\utils\crscrcomp.exe in=test1.crscr out=test1.crscr.bin
```

The resulting `test1.crscr.bin` takes 86 bytes.

Everyting is included from `test.bas`. Note how the contents of meta_ross.txt (used in `crscr.exe` to create the background) is the same you can find in the `metatileSetTest` label. The charset is `IncBin`'d balow a `charsetTest` label. So this is how you initialize the library:

```
crscrInit (@charsetTest, @metatileSetTest)
```

Our background is `IncBin`'d below a `screenTest1` label. To render it:

```
crscrRender (@screenTest1)
```

Note that you must paint the screen black before displaying the backgrounds.
