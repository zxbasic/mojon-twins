' crscrcomp v0.1.20171004
' Compiles .crscr screens into packed binary data

#include "cmdlineparser.bi"
#include "mtfuncs.bi"

Type PaintCommandType
	opcode as uByte
	x1 as uByte
	y1 as uByte
	x2 as uByte
	y2 as uByte
	c as uByte
End Type

Dim Shared As PaintCommandType paintCommands (255)
Dim Shared As Integer paintCommandsIndex

Sub usage
	Print "$ crscrcomp.exe in=screen.crscr out=screen.bin"
End Sub

Sub loadMyStuff (fName As String)
	Dim As Integer fIn
	Dim As uByte d
	fIn = FreeFile
	Open fName For Binary As #fIn
	paintCommandsIndex = 0
	Do
		Get #fIn, , d
		If d = &HFF Then Exit Do
		paintCommands (paintCommandsIndex).opcode = d
		Get #fIn, , paintCommands (paintCommandsIndex).x1
		Get #fIn, , paintCommands (paintCommandsIndex).y1
		Get #fIn, , paintCommands (paintCommandsIndex).x2
		Get #fIn, , paintCommands (paintCommandsIndex).y2
		Get #fIn, , paintCommands (paintCommandsIndex).c
		paintCommandsIndex = paintCommandsIndex + 1
	Loop	
	Close #fIn
End Sub

Dim As String mandatory (1) = {"in", "out"}
Dim As Integer fOut, bytesWritten, i
Dim As uByte d

Print "crscrcomp v0.1.20171004 ";

sclpParseAttrs
If Not sclpCheck (mandatory ()) Then usage: End

loadMyStuff sclpGetValue ("in")

fOut = FreeFile
Kill sclpGetValue ("out")
Open sclpGetValue ("out") For Binary As #fOut
bytesWritten = 0

For i = 0 To paintCommandsIndex - 1
	Select Case paintCommands (i).opcode
		Case 0:
			' ATTR
			d = 0: Put #fOut, , d
			Put #fOut, , paintCommands (i).c
			bytesWritten = bytesWritten + 2
		Case 4:
			' PUTCHAR 0100XXXX X··YYYYY CCCCCCCC
			d = (4 Shl 4) Or (paintCommands (i).x1 Shr 1)
			Put #fOut, , d 

			d = ((paintCommands (i).x1 And 1) Shl 7) Or paintCommands (i).y1
			Put #fOut, , d

			Put #fOut, , paintCommands (i).c
			bytesWritten = bytesWritten + 3
		Case 5:
			' RECTCHAR 0101XXXX XYYYYYXX XXXYYYYY ·CCCCCCC
			d = (5 Shl 4) Or (paintCommands (i).x1 Shr 1)
			Put #fOut, , d

			d = ((paintCommands (i).x1 And 1) Shl 7) Or (paintCommands (i).y1 Shl 2) Or (paintCommands (i).x2 Shr 3)
			Put #fOut, , d 

			d = ((paintCommands (i).x2 And 7) Shl 5) Or paintCommands (i).y2
			Put #fOut, , d

			Put #fOut, , paintCommands (i).c
			bytesWritten = bytesWritten + 4
		Case 6:
			' PUTBLOCK 0100XXXX X··YYYYY CCCCCCCC
			d = (6 Shl 4) Or (paintCommands (i).x1 Shr 1)
			Put #fOut, , d 

			d = ((paintCommands (i).x1 And 1) Shl 7) Or paintCommands (i).y1
			Put #fOut, , d

			Put #fOut, , paintCommands (i).c
			bytesWritten = bytesWritten + 3
		Case 7:
			' RECTBLOCK 0101XXXX XYYYYYXX XXXYYYYY ·CCCCCCC
			d = (7 Shl 4) Or (paintCommands (i).x1 Shr 1)
			Put #fOut, , d

			d = ((paintCommands (i).x1 And 1) Shl 7) Or (paintCommands (i).y1 Shl 2) Or (paintCommands (i).x2 Shr 3)
			Put #fOut, , d 

			d = ((paintCommands (i).x2 And 7) Shl 5) Or paintCommands (i).y2
			Put #fOut, , d

			Put #fOut, , paintCommands (i).c
			bytesWritten = bytesWritten + 4
		Case 8:
			' PUTMETA 0100XXXX X··YYYYY CCCCCCCC
			d = (8 Shl 4) Or (paintCommands (i).x1 Shr 1)
			Put #fOut, , d 

			d = ((paintCommands (i).x1 And 1) Shl 7) Or paintCommands (i).y1
			Put #fOut, , d

			Put #fOut, , paintCommands (i).c
			bytesWritten = bytesWritten + 3
		Case 9:
			' RECTMETA 0101XXXX XYYYYYXX XXXYYYYY ·CCCCCCC
			d = (9 Shl 4) Or (paintCommands (i).x1 Shr 1)
			Put #fOut, , d

			d = ((paintCommands (i).x1 And 1) Shl 7) Or (paintCommands (i).y1 Shl 2) Or (paintCommands (i).x2 Shr 3)
			Put #fOut, , d

			d = ((paintCommands (i).x2 And 7) Shl 5) Or paintCommands (i).y2
			Put #fOut, , d

			Put #fOut, , paintCommands (i).c
			bytesWritten = bytesWritten + 4
	End Select
Next i
d = &HFF: Put #fOut, , d
bytesWritten = bytesWritten + 1

Close #fOut

Print "~ DONE! L=" & bytesWritten
