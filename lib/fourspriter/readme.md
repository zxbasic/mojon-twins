fourspriter
===========

**Fourspriter** is a very simple, automaticly driven library which lets you control up to 4 multicoloured 16x16 pixels sprites on screen and takes care of restoring the background. Sprites move in character steps. 

The library is fast enough to produce 25 fps games, which is more than enough for stuff moving around in character steps.

You can check the source code of [Phantomas V 2](https://bitbucket.org/zxbasic/mojon-twins/src/3f69d7afb360b427e38283c9af2c8e589a66e742/games/Phantomas_V_2/?at=master) to see it in action.

Short reference
===============

Configuring Gfx Address
-----------------------

Fourspriter can make its sprites from a set of 256 characters you can place anywhere on RAM. The first thing you should do is specify where it is calling

```BASIC
	fsp21SetGfxAddress (address)
```

Of course, you can define your graphics as an array in ZX Basic:

```BASIC
	fsp21SetGfxAddress (@spriteset (0))
```

Placing and configuring sprites
-------------------------------

So there are four sprites, numbered 0-3.

You can activate or deactivate them at any time. The engine update function will only display the active sprites:

```BASIC
	fsp21ActivateSprite (n)
	fsp21DeactivateSprite (n)
```

Sprite faces are composed by a 16x16 graphic and an INK colour (the library takes the PAPER and BRIGHT from the screen and only modifies the INK). You can change each one of the four attributes which colour the sprite individually (from top to bottom, from left to right).

To change colour of sprite n:

```BASIC
	fsp21ColourSprite (n, attr1, attr2, attr3, attr4)
```

To define the sprite face, you must specify which 4 characters (from top to bottom, from left to right) from the assigned charset will be used:

```BASIC
	fsp21SetGfxSprite (n, attr1, attr2, attr3, attr4)
```

Last, but not least, you need to tell the engine where the sprites are:

```BASIC
	fsp21MoveSprite (n, x, y)
```

If this is the first time you "move" your sprite (that is, if you are 'setting' where your sprite is, for example, when entering a new screen) you need to call this so some buffers get properly initializated:

```BASIC
	fsp21DuplicateCoordinatesSprite (n)
```

Initializing the engine
-----------------------

Once all sprites are in place, active and defined, you must call this so the library prepares some stuff:

```BASIC
	fsp21InitSprites ()
```

Main loop
---------

In your main loop, you usually:

- Change your sprite faces and / or colours using `fsp21SetGfxSprite` and `fsp21ColourSprite`.
- Move your sprites around using `fsp21MoveSprite`.
- Then call the update function:

```BASIC
	fsp21UpdateSprites ()
```

You may want to add some `halt` or timing stuff before. I usually introduce a dummy loop to make everything happen at the beginning of the lower border so I have plenty of time to move stuff around without flickering.

Short example
=============

This is a very basic example which moves around one sprite (sprite 3). For more complex stuff, check a [proper game](https://bitbucket.org/zxbasic/mojon-twins/src/3f69d7afb360b427e38283c9af2c8e589a66e742/games/Phantomas_V_2/?at=master).

```BASIC

#include once "fsp2.1.bas"
#include once "spriteset.bas"

Dim x, y, mx, my as uByte

Border 0: Paper 0: Ink 7: Cls
centrar (11, "PRUEBA FOURSPRITER")
centrar (12, "POR MOJONOMOR")

x = 0: y = 0
mx = 1: my = 1

fsp21SetGfxAddress (@spriteset (0))

fsp21SetGfxSprite (3, 0, 1, 2, 3)
fsp21ColourSprite (3, 71, 70, 70, 70)

fsp21MoveSprite (3, x, y)
fsp21ActivateSprite (3)

fsp21InitSprites ()

While Inkey = ""
	x = x + mx
	y = y + my
	
	If x = 0 Or x = 30 Then 
		mx = -mx
	End If
	
	If y = 0 Or y = 22 Then
		my = -my
	End If

	fsp21MoveSprite (3, x, y)

	Asm
		halt
		halt
	End Asm
	
	fsp21UpdateSprites ()
Wend

End

Sub centrar (y as uByte, t as String)
   Print At y, 16 - len (t) / 2; t
End Sub
```
