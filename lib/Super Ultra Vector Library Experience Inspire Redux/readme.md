Super Ultra Vector Library Experience Inspire Redux
===================================================

Super Ultra Vector Library Experience Inspire Redux is a simple library to load and display vector art pictures in your ZX Basic games. It comes with a horrific, unstable, dreadful vector editor program to create such pictures. 

Pictures are saved in a mildly compact binary format you can include from your ZX Basic games and feed it to the library for display.

This library makes use of `SPPFill.bas`, which contains Alvin Albrecht's patterned flood fill routine as ported to ZX Basic by Britlion.

DRAW
----

DRAW is the crappy vector graphics editor. It's incredibly dull but can make the job quite easy. You can trace over a background image if you include such image as "fondo.pcx" in the same folder as "draw.exe". The image should be 256x192 pixels.

The included "fondo.pcx" is just a 8x8 grid. I use it all the time.

- Press `0`-`7` to select one of 8 patterns. They show as plain colours in the editor, though. As patterns can be whatever (you define them in the library, more about this later), this is pretty convenient.
- Press `D` to show / hide "fondo.pcx". Useful for tracing.
- Press `F`, then click somewhere for a flood fill using the currently selected pattern.
- Press `U` to undo last action. You can undo everything if you like. 
- Press `G` to enter the *hardness map mode*. 
- Press `S` to save the current image + hardness map (if available) as `importme.bin`.
- *Right click* establishes the latest plot point.
- *Left click* draws a line from the latest plot to the current mouse position.

For Sinclair BASIC coders, *Right click* = PLOT; *Left click* = DRAW.

The hardess map is there so you can chose which chars are waklable and which aren't in case you want to use the vector image as a background (as I did, just check **Lhikoj!**)

**First thing you should do after saving is renaming importme.bin**

The library
-----------

1.- Copy the files `gfxParser.bas` and `SPPFill.bas` to your project folder. They are located in the src folder.

2.- Copy your vector image binaries (this is, your renamed `importme.bin` droppins from DRAW) to your project folder.

3.- Include your vector image binaries in your code, for example this way:

```BASIC
	' Images
	Sub imageContainer
		image001:
		Asm
		   IncBin "image001.bin"
		End Asm
	End Sub
```

4.- Include gfxparser and screens.bas in your main file:

```
	#include once "gfxParser.bas"
	#include once "screens.bas"
```

5.- To render a image, just call gpParseBinary passing the address where the image is as a parameter. In this case, the address of the label you defined before the assembly block, that is,

```
	gpParseBinary (@image001)
```

6.- To read the hardness layer, just call gpIsHard passing the oordinates:

```
	If gpisHard (x, y) Then Print "Obstacle": End If
```

Feel free to update/enhance the library. The library contains old code written for old, less featured versions of ZX Basic, so it might benefit from a revision.

Links
-----

* Tutorial [in Spanish] https://tcyr.wordpress.com/2012/03/07/suvleir-3-0-super-ultra-vector-library-experience-inspire-redux-3-0/
