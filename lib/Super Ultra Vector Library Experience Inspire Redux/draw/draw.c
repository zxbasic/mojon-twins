// Version 3: Added hardness layer
// binary structure:
// first two bytes: hardness layer offset
// Then, vectors.
// Then, hardness layer (RLE-compressed).

#include <allegro.h>
#include <stdio.h>

typedef struct {
	int op;
	int x;
	int y;
	int ex;
} OPERACIONES;

int main (void) {
	BITMAP *buffer;
	BITMAP *bufin;
	BITMAP *gridbit;
	BITMAP *rebuffer;
	OPERACIONES ops [65536];
	int idx = 0;
	int last_x, last_y, x, y, i;
	int cx,cy;
	int xx, yy;
	unsigned char mode = 0;
	unsigned char clr = 0;
	unsigned int colores[8];
	BITMAP *fondo;
	unsigned char fondo_activo = 0;
	FILE *pf;
	unsigned char grid_active = 0;
	unsigned char grid [24][32];
	unsigned char my_binary [8192];
	unsigned int my_binary_pointer;
	unsigned char act_v;
	unsigned char act_ct;
	unsigned char lsb, msb, c, d;
	
	allegro_init ();
	install_keyboard ();
	install_mouse ();
	
	set_color_depth (32);
	set_gfx_mode(GFX_AUTODETECT_WINDOWED,640,480,0,0);
	
	colores[0]=makecol (0,0,0);
	colores[1]=makecol(0,0,255);
	colores[2]=makecol(255,0,0);
	colores[3]=makecol(255,0,255);
	colores[4]=makecol(0,255,0);
	colores[5]=makecol(0,255,255);
	colores[6]=makecol(255,255,0);
	colores[7]=makecol(255,255,255);
	
	fondo = load_pcx ("fondo.pcx", NULL);
		
	buffer = create_bitmap (256, 192);
	gridbit = create_bitmap (256, 192);	
	bufin = create_bitmap (8,8);
	clear_to_color (buffer, makecol (255, 255, 255));
	clear_to_color (gridbit, makecol (255, 0, 255));
	
	for (i=0; i < 256; i=i+8)
		vline(gridbit, i, 0, 191, makecol(191,191,191));
		
	for (i=0; i < 192; i=i+8)
		hline(gridbit, 0, i, 255, makecol(191,191,191));
		
	cx = 8; cy = 8;
	blit(buffer, bufin, cx - 3, cy - 3, 0, 0, 8, 8);
	
	clear_to_color (screen, makecol (50,100,200));
	
	textprintf_ex (screen, font, 0, 460, makecol (200,255,255),-1,"LCLICK: Set endpoint, RCLICK: Set startpoint, D shows 'fondo.pcx', U undo");
    textprintf_ex (screen, font, 0, 470, makecol (200,255,255),-1,"F: Next LCLICK fills, 0-7: Pattern, G: Hardness map, S: Save 'import.bin' & ESC: Exit");
    
    textprintf_ex (screen, font, 2, 2, makecol (255, 255, 100), -1, "SUVLEIR3");
    textprintf_ex (screen, font, 80, 2, makecol (200, 200, 200), -1, "copyleft MOJON TWINS 2010-12");
	
	while (!key[KEY_ESC]) {
		x = mouse_x/2 -1;
		y = mouse_y/2 -1;
	
			stretch_blit (buffer, screen, 0, 0, 256, 192, 20, 16, 512, 384);
			if (grid_active)
               masked_stretch_blit (gridbit, screen, 0, 0, 255, 192, 20, 16, 512, 384);
			
			textprintf_ex (screen, font, 0, 406, makecol(255,255,255), makecol (0,100,200), "[%3d, %3d]", x, y);
			textprintf_ex (screen, font, 0, 416, makecol(255,255,255), makecol (0,100,200), "IDX %d", idx);
			textprintf_ex (screen, font, 48, 416, makecol(255,255,255), makecol (0,100,200), "MODE %d", mode);
			textprintf_ex (screen, font, 127, 406, colores[clr], makecol (0,100,200), "PATTERN %d", clr);
			
			blit (bufin, buffer, 0, 0, cx - 3, cy - 3, 8, 8);
			blit (buffer, bufin, x - 3, y - 3, 0, 0, 8, 8);
			
			hline (buffer, x-3, y, x-1, makecol(255,0,0));
			hline (buffer, x+1, y, x+3, makecol(255,0,0));
			vline (buffer, x, y-3, y-1, makecol(255,0,0));
			vline (buffer, x, y+1, y+2, makecol(255,0,0));
			
			cx = x;
			cy = y;
		
		if (mouse_b & 1) {
			
			if (!grid_active) {
    			switch (mode) {
    				case 0:
    					// Plot
    					blit (bufin, buffer, 0, 0, cx - 3, cy - 3, 8, 8);
    					putpixel (buffer, x, y, 0);
    					last_x = x;
    					last_y = y;
    					ops [idx].op = 0;
    					ops [idx].x = x;
    					ops [idx].y = y;
    					ops [idx].ex = 0;
    					mode = 1;	
    					blit (buffer, bufin, x - 3, y - 3, 0, 0, 8, 8);
    
    					break;
    				case 1:
    					// Draw
    					blit (bufin, buffer, 0, 0, cx - 3, cy - 3, 8, 8);
    					line (buffer, last_x, last_y, x, y, 0);
    					ops [idx].op = 1;
    					ops [idx].x = x;
    					ops [idx].y = y;
    					ops [idx].ex = 0;
    					last_x = x;
    					last_y = y;
    					blit (buffer, bufin, x - 3, y - 3, 0, 0, 8, 8);
    
    					break;
    				case 2:
    					// Fill
    					blit (bufin, buffer, 0, 0, cx - 3, cy - 3, 8, 8);
    					floodfill (buffer, x, y, colores[clr]);
    					ops [idx].op = 2;
    					ops [idx].x = x;
    					ops [idx].y = y;
    					ops [idx].ex = clr;
    					mode = 0;
    					blit (buffer, bufin, x - 3, y - 3, 0, 0, 8, 8);
    
					break;
                }
                while (mouse_b & 1);
    			idx ++;
            } else {
                xx = x >> 3;
                yy = y >> 3;
                grid [yy][xx] = 1;
                xx = xx << 3;
                yy = yy << 3;
                rectfill (gridbit, xx + 1, yy + 1, xx + 7, yy + 7, makecol (0, 0, 255));
            }
							
			
		}
		
		if (mouse_b & 2) {
            if (!grid_active) {
    			mode = 0;
            } else {
                xx = x >> 3;
                yy = y >> 3;
                grid [yy][xx] = 0;
                xx = xx << 3;
                yy = yy << 3;
                rectfill (gridbit, xx + 1, yy + 1, xx + 7, yy + 7, makecol (255, 0, 255));
            }
		}
		
		if (key [KEY_F]) {
			mode = 2;
		}
		
		if (key [KEY_G]) {
            grid_active = !grid_active;
            while (key [KEY_G]);
        }
		
		if (key [KEY_0])
			clr = 0;
		if (key [KEY_1])
			clr = 1;
		if (key [KEY_2])
			clr = 2;
		if (key [KEY_3])
			clr = 3;
		if (key [KEY_4])
			clr = 4;
		if (key [KEY_5])
			clr = 5;
		if (key [KEY_6])
			clr = 6;
		if (key [KEY_7])
			clr = 7;
			
		if (key [KEY_U] && idx > 0) {
			idx --;
			clear_to_color (buffer, makecol (255, 255, 255));
			if (fondo_activo) 
				blit (fondo, buffer, 0, 0, 0, 0, 256, 192);
			for (i = 0; i < idx; i++) {
				switch (ops[i].op) {
					case 0:
						last_x = ops[i].x;
						last_y = ops[i].y;
					case 1:
						line (buffer, last_x, last_y, ops[i].x, ops[i].y,0);
						last_x = ops[i].x;
						last_y = ops[i].y;
					case 2:	
						floodfill(buffer, ops[i].x, ops[i].y, colores[ops[i].ex]);
				}	
			}	
			blit (buffer, bufin, x - 3, y - 3, 0, 0, 8, 8);
			while (key[KEY_U]);
		}
		
		if (key [KEY_D]) {
			fondo_activo = !fondo_activo;
			clear_to_color (buffer, makecol (255, 255, 255));
			if (fondo_activo) 
				blit (fondo, buffer, 0, 0, 0, 0, 256, 192);
			for (i = 0; i < idx; i++) {
				switch (ops[i].op) {
					case 0:
						last_x = ops[i].x;
						last_y = ops[i].y;
					case 1:
						line (buffer, last_x, last_y, ops[i].x, ops[i].y,0);
						last_x = ops[i].x;
						last_y = ops[i].y;
					case 2:	
						floodfill(buffer, ops[i].x, ops[i].y, colores[ops[i].ex]);
				}	
			}	
			blit (buffer, bufin, x - 3, y - 3, 0, 0, 8, 8);
			while (key [KEY_D]);
		}
		
		if (key [KEY_L]) {
            pf = fopen ("importme.bin", "rb");
            idx = 0;
            lsb = fgetc (pf);
            msb = fgetc (pf);
            my_binary_pointer = lsb + (msb << 8);
            i = 0;
            while (i < my_binary_pointer) {
                c = fgetc (pf); i++;
                if (c == 192) {
                    ops [idx].op = 0;
                    ops [idx].x = fgetc (pf);
                    ops [idx].y = fgetc (pf);
                    i += 2;
                    idx ++;
                } else if (c == 193) {
                    ops [idx].op = 2;
                    ops [idx].x = fgetc (pf);
                    ops [idx].y = fgetc (pf);
                    ops [idx].ex = fgetc (pf);
                    i += 3;
                    idx ++;
                } else {
                    ops [idx].op = 1;
                    ops [idx].y = c;
                    ops [idx].x = fgetc (pf);
                    i ++;
                    idx ++;
                }
            }
            fclose (pf);
            clear_to_color (buffer, makecol (255, 255, 255));
			if (fondo_activo) 
				blit (fondo, buffer, 0, 0, 0, 0, 256, 192);
			for (i = 0; i < idx; i++) {
				switch (ops[i].op) {
					case 0:
						last_x = ops[i].x;
						last_y = ops[i].y;
					case 1:
						line (buffer, last_x, last_y, ops[i].x, ops[i].y,0);
						last_x = ops[i].x;
						last_y = ops[i].y;
					case 2:	
						floodfill(buffer, ops[i].x, ops[i].y, colores[ops[i].ex]);
				}	
			}	
			blit (buffer, bufin, x - 3, y - 3, 0, 0, 8, 8);
			textprintf_ex (screen, font, 320, 406, makecol(255,255,255), makecol (0,100,200), "%d %d %d", lsb, msb, my_binary_pointer);
        }
		
		if (key [KEY_S]) {
            // Export:
        	//ops [idx].op = 3; 	// marca fin
        	
        	pf = fopen ("importme.bin", "wb");
        	
        	my_binary_pointer = 2;
        	
        	for (i = 0; i <= idx; i ++) {
        		if (ops[i].x>255) ops[i].x=255;
        		if (ops[i].y>191) ops[i].y=191;
        		if (ops[i].x<0) ops[i].x = 0;
        		if (ops[i].y<0) ops[i].y = 0;
        		
        	    // La version 2.0 comprime un poco m�s los datos, de esta forma:
                // Y :: Si es < 192, entonces leer un byte m�s (X) y es un DRAW
                //      Si es = 192, entonces leer dos bytes m�s (X,Y) y es un PLOT
                //      Si es = 193, entonces leer tres bytes m�s (X,Y,P) y es un FILL
                //      Si es = 194, entonces leer un byte m�s (C) y es un INK (para el futuro)
                
                if (ops [i].op == 1) {
                    /*
                    fputc ((unsigned char)ops [i].y, pf);
                    fputc ((unsigned char)ops [i].x, pf);
                    */
                    my_binary [my_binary_pointer ++] = (unsigned char) ops [i].y;
                    my_binary [my_binary_pointer ++] = (unsigned char) ops [i].x;
                } else if (ops [i].op == 0) {
                    /*
                    fputc ((unsigned char) 192, pf);
                    fputc ((unsigned char)ops [i].x, pf);
                    fputc ((unsigned char)ops [i].y, pf);
                    */
                    my_binary [my_binary_pointer ++] = (unsigned char) 192;
                    my_binary [my_binary_pointer ++] = (unsigned char) ops [i].x;
                    my_binary [my_binary_pointer ++] = (unsigned char) ops [i].y;
                } else if (ops [i].op == 2) {
                    /*
                    fputc ((unsigned char) 193, pf);
                    fputc ((unsigned char)ops [i].x, pf);
                    fputc ((unsigned char)ops [i].y, pf);
                    fputc ((unsigned char)ops [i].ex, pf);
                    */
                    my_binary [my_binary_pointer ++] = (unsigned char) 193;
                    my_binary [my_binary_pointer ++] = (unsigned char) ops [i].x;
                    my_binary [my_binary_pointer ++] = (unsigned char) ops [i].y;
                    my_binary [my_binary_pointer ++] = (unsigned char) ops [i].ex;
                } else if (ops [i].op == 3) {
                    //fputc ((unsigned char) 255, pf);
                }
        	}	
        	
        	my_binary [0] = (unsigned char) (my_binary_pointer & 255);
        	my_binary [1] = (unsigned char) (my_binary_pointer >> 8);
        	
        	// RLE-ENCODE hardness layer
        	
        	xx = 0; yy = 0;
        	act_v = 255;
        	act_ct = 0;
        	for (i = 0; i < 768; i ++) {
                if (grid [yy][xx] != act_v || act_ct >= 63 || i == 767) {
                    if (act_ct != 0) {
                        my_binary [my_binary_pointer ++] = (unsigned char) ((act_ct & 63) | (act_v << 6));
                    }
                    act_ct = 1;
                    act_v = grid [yy][xx];
                } else {
                    act_ct ++;
                }
                xx ++; if (xx == 32) {yy ++; xx = 0;}
            }
            
            // Write to file
            
            for (i = 0; i < my_binary_pointer; i ++)
                fputc ((unsigned char) my_binary [i], pf);
        	
        	fclose (pf);    
        }
		
		vsync ();
	}
	
	
	
	allegro_exit ();	
} END_OF_MAIN ();
