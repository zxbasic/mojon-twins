' subacolib 
'
' ====================================================
' Super Babalizoominator Construction Kit for ZX Basic
' ====================================================
'
' This library was created with games like Saimazoom or Babaliba in mind,
' but it should be usable for anything.
'
' It features: 
' * Support for a tileset containing up to 256 24x24 pixel coloured tiles.
' * Direct print any coloured tile on screen character coordinates.
' * Up to three sprites which don't erase the background.
'
' The sprite routines take like 1.5 of border time to execute. That's why 
' we have included two versions:
'
' * The normal one will run after a HALT. If you activate more than two 
'   sprites the third will blink when it's on the top five character lines.
' * The delayed one will run after a HALT and a loop to wait until the raster
'   reaches the bottom border. There's no flickering this way but your game
'   will run at 25fps at most.
'
' I might write a tutorial if anybody is interested. Functions are 
' self-explanatory. If you have worked with fourspriter this works in 
' a very simmiliar fashion.
'
' This library needs a tileset .bas file. Tilesets can be generated using the
' 24x24.exe program included alongside this library.
'
' copyleft 2012 the Mojon Twins

' How to use this:
'
' 1. First draw your backdrop using subacoDrawTile calls. 
' 2. Then decide which sprites will be active using subacoActivateSprite.
' 3. Then assign a tile to each sprite. This tile can be changed at any
'    time to animate them. Use subacoDefSprite
' 4. Then assign the coordinates to each sprite using subacoMoveSprite
' 5. Now call subacoInit.
' 6. Time for your game loop.
' 7. Update your coordinates, and then call subacoMoveSprite with them.
' 8. Call subacoMoveSpritesLowBorder to make it show.
' 9. Back to 6.
' 10. Profit.

' Check testlib.bas for a very dumb demonstration.

' This sub draws tile #tile on screen at (x, y)
Sub subacoDrawTile (x As uByte, y As uByte, tile As uByte)
	Poke @subacoDataPool, x
	Poke 1 + @subacoDataPool, y
	Poke 2 + @subacoDataPool, tile
Asm
		call subaco_draw_tile
End Asm
End Sub

' This sub animates the active sprites (normal version, flickers)
Sub Fastcall subacoMoveSprites ()
Asm		
	subaco_move_sprites:
		halt
		call subaco_restore_background
		call subaco_fill_buffers
		call subaco_draw_sprites
		call subaco_update_coordinates
		ret
End Asm
End Sub

' This sub animates the active sprite (delayed version, flickerless)
Sub Fastcall subacoMoveSpritesLowBorder ()
Asm		
	subaco_move_sprites_low_border:
		halt

	;; This UGLY code wastes times until raster reaches bottom border		
		ld bc, 1900
	loop_waste: 
		dec bc
		ld a, b
		cp 0
		jp nz, loop_waste
		ld a, c
		cp 0
		jp nz, loop_waste
	;; Suggestions to make this look better, please.
		
		call subaco_restore_background
		call subaco_fill_buffers
		call subaco_draw_sprites
		call subaco_update_coordinates
		ret
End Asm
End Sub

' Call this right after your backdrop is fully drawn. It fills the
' sprites background buffers.
Sub Fastcall subacoInit ()
Asm
	call subaco_fill_buffers
	call subaco_update_coordinates
End Asm
End Sub

' Use this to assign a #tile to sprite #n (0, 1 or 2). The sprite
' will be drawn using such tile.
Sub subacoDefSprite (n as uByte, ntile as uByte)
	Dim realAddress as uInteger
	realAddress = Cast (uInteger, ntile) * 81 + @tileSet24x24
	If n = 0 Then
		Poke uInteger 2 + @sbcSprite0Data, realAddress
	ElseIf n = 1 Then
		Poke uInteger 2 + @sbcSprite1Data, realAddress
	ElseIf n = 2 Then
		Poke uInteger 2 + @sbcSprite2Data, realAddress
	End If
End Sub

' Activate sprite #n so it shows.
Sub subacoActivateSprite (n as uByte)
	If n = 0 Then
		Poke 6 + @sbcSprite0Data, 1
	ElseIf n = 1 Then
		Poke 6 + @sbcSprite1Data, 1
	ElseIf n = 2 Then
		Poke 6 + @sbcSprite2Data, 1
	End If
End Sub

' Dectivate sprite #n so it doesn't show.
Sub subacoDeactivateSprite (n as uByte)
	If n = 0 Then
		Poke 6 + @sbcSprite0Data, 0
	ElseIf n = 1 Then
		Poke 6 + @sbcSprite1Data, 0
	ElseIf n = 2 Then
		Poke 6 + @sbcSprite2Data, 0
	End If
End Sub

' Move sprite 0 to (x, y)
Sub subacoMoveSprite (n as uByte, x as uByte, y as uByte)
	If n = 0 Then
		Poke @sbcSprite0Data, x
		Poke 1 + @sbcSprite0Data, y
	ElseIf n = 1 Then
		Poke @sbcSprite1Data, x
		Poke 1 + @sbcSprite1Data, y
	ElseIf n = 2 Then
		Poke @sbcSprite2Data, x
		Poke 1 + @sbcSprite2Data, y
	End If
End Sub

' Now there's a bunch of buffers and ugly, ugly code. 
Sub subacoCodeContainer ()
subacoDataPool:
Asm
	subaco_data_pool:
	sbc_x:	defb 0
	sbc_y:	defb 0
	sbc_n:	defb 0
	subaco_line_buffer:
		defw $4000,$4020,$4040,$4060,$4080,$40A0,$40C0,$40E0
		defw $4800,$4820,$4840,$4860,$4880,$48A0,$48C0,$48E0
		defw $5000,$5020,$5040,$5060,$5080,$50A0,$50C0,$50E0
	subaco_sprite_buffers:
	subaco_sprite_buffer_0:
		defs 81, 0
	subaco_sprite_buffer_1:
		defs 81, 0
	subaco_sprite_buffer_2:
		defs 81, 0
	; For each sprite: x, y, cx, cy, n. n contains the actual address!
End Asm
sbcSprite0Data:
Asm
	sbc_x0:	defb 0
	sbc_y0:	defb 0
	sbc_n0: defw 0
	sbccx0: defb 0
	sbccy0: defb 0
	sbc_a0: defb 0
End Asm
sbcSprite1Data:
Asm
	sbc_x1:	defb 0
	sbc_y1:	defb 0
	sbc_n1: defw 0
	sbccx1: defb 0
	sbccy1: defb 0
	sbc_a1: defb 0
End Asm
sbcSprite2Data:
Asm
	sbc_x2:	defb 0
	sbc_y2:	defb 0
	sbc_n2: defw 0
	sbccx2: defb 0
	sbccy2: defb 0
	sbc_a2: defb 0
		defb 1,2,3,4,5,6,7,8
	subaco_draw_tile:
		; First compute tile address
		; tileset_24x24 + sbc_n * 81.
		; sbc_n * 81 = sbc_n * 64 + sbc_n * 16 + sbc_n
		ld	a, (sbc_n)
		ld	h, 0
		ld	l, a
		push hl
		add	hl, hl
		add	hl, hl
		add	hl, hl
		add	hl, hl		; HL = sbc_n * 16
		push hl
		add	hl, hl
		add	hl, hl		; HL = sbc_n * 64
		pop	bc
		add	hl, bc		; HL = sbc_n * 64 + sbc_n * 16
		pop	bc
		add hl, bc		; HL = sbc_n * 64 + sbc_n * 16 + sbc_n
		ld	bc, tileset_24x24
		add	hl, bc
	subaco_draw_tile_in_hl:	
		; Now compute screen address
		push hl
		ld	a, (sbc_y)
		add	a, a
		ld	h, 0
		ld	l, a
		ld	bc, subaco_line_buffer
		add	hl, bc
		ld	e, (hl)
		inc	hl
		ld	d, (hl)
		ex	de, hl
		ld	a, (sbc_x)
		ld	d, 0
		ld 	e, a
		add	hl, de
		ex	de, hl		
		pop hl

		; Now copy
		ldi
		ldi
		ldi
		dec	e
		dec e
		dec	e
		inc	d
		ldi
		ldi
		ldi
		dec	e
		dec e
		dec	e
		inc	d
		ldi
		ldi
		ldi
		dec	e
		dec e
		dec	e
		inc	d
		ldi
		ldi
		ldi
		dec	e
		dec e
		dec	e
		inc	d
		ldi
		ldi
		ldi
		dec	e
		dec e
		dec	e
		inc	d
		ldi
		ldi
		ldi
		dec	e
		dec e
		dec	e
		inc	d
		ldi
		ldi
		ldi
		dec	e
		dec e
		dec	e
		inc	d
		ldi
		ldi
		ldi
		
		push hl
		
		; Now compute screen address
		ld	a, (sbc_y)
		inc	a
		add	a, a
		ld	h, 0
		ld	l, a
		ld	bc, subaco_line_buffer
		add	hl, bc
		ld	e, (hl)
		inc	hl
		ld	d, (hl)
		ex	de, hl
		ld	a, (sbc_x)
		ld	d, 0
		ld 	e, a
		add	hl, de
		ex	de, hl
		
		pop hl
		
		; Now copy
		ldi
		ldi
		ldi
		dec	e
		dec e
		dec	e
		inc	d
		ldi
		ldi
		ldi
		dec	e
		dec e
		dec	e
		inc	d
		ldi
		ldi
		ldi
		dec	e
		dec e
		dec	e
		inc	d
		ldi
		ldi
		ldi
		dec	e
		dec e
		dec	e
		inc	d
		ldi
		ldi
		ldi
		dec	e
		dec e
		dec	e
		inc	d
		ldi
		ldi
		ldi
		dec	e
		dec e
		dec	e
		inc	d
		ldi
		ldi
		ldi
		dec	e
		dec e
		dec	e
		inc	d
		ldi
		ldi
		ldi

		push hl
			
		; Now compute screen address
		ld	a, (sbc_y)
		inc	a
		inc	a
		add	a, a
		ld	h, 0
		ld	l, a
		ld	bc, subaco_line_buffer
		add	hl, bc
		ld	e, (hl)
		inc	hl
		ld	d, (hl)
		ex	de, hl
		ld	a, (sbc_x)
		ld	d, 0
		ld 	e, a
		add	hl, de
		ex	de, hl
		
		pop hl
		
		; Now copy
		ldi
		ldi
		ldi
		dec	e
		dec e
		dec	e
		inc	d
		ldi
		ldi
		ldi
		dec	e
		dec e
		dec	e
		inc	d
		ldi
		ldi
		ldi
		dec	e
		dec e
		dec	e
		inc	d
		ldi
		ldi
		ldi
		dec	e
		dec e
		dec	e
		inc	d
		ldi
		ldi
		ldi
		dec	e
		dec e
		dec	e
		inc	d
		ldi
		ldi
		ldi
		dec	e
		dec e
		dec	e
		inc	d
		ldi
		ldi
		ldi
		dec	e
		dec e
		dec	e
		inc	d
		ldi
		ldi	
		ldi
		
		; Copy colours
		
		; de = 22528 + y * 32 + x
		push hl
		ld	a, (sbc_y)
		ld	h, 0
		ld	l, a
		add	hl, hl
		add	hl, hl
		add	hl, hl
		add	hl, hl
		add	hl, hl
		ld	a, (sbc_x)
		ld	d, 0
		ld	e, a
		add	hl, de
		ld	bc, 22528
		add	hl, bc
		ex	de, hl
		pop	hl
		
		ldi
		ldi
		ldi
		ex	de, hl
		ld	bc, 29
		add	hl, bc
		ex	de, hl
		ldi
		ldi
		ldi
		ex	de, hl
		ld	bc, 29
		add	hl, bc
		ex	de, hl
		ldi
		ldi
		ldi
		
		ret
	
	; Now a routine to copy from screen to a buffer	
	; Buffer in DE
	; Coordinates in B = y, C = x
	subaco_copy_to_buffer:
		push de
		ld	a, b
		add a, a
		ld	d, 0
		ld	e, a
		ld	hl, subaco_line_buffer
		add	hl, de
		ld	e, (hl)
		inc	hl
		ld	d, (hl)
		ex	de, hl
		ld	d, 0
		ld	e, c
		add	hl, de
		pop de
		
		push bc
		ldi
		ldi
		ldi
		dec	l
		dec l
		dec	l
		inc	h
		ldi
		ldi
		ldi
		dec	l
		dec l
		dec	l
		inc	h
		ldi
		ldi
		ldi
		dec	l
		dec l
		dec	l
		inc	h
		ldi
		ldi
		ldi
		dec	l
		dec l
		dec	l
		inc	h
		ldi
		ldi
		ldi
		dec	l
		dec l
		dec	l
		inc	h
		ldi
		ldi
		ldi
		dec	l
		dec l
		dec	l
		inc	h
		ldi
		ldi
		ldi
		dec	l
		dec l
		dec	l
		inc	h
		ldi
		ldi
		ldi
		pop	bc
		
		push de
		ld	a, b
		inc a
		add a, a
		ld	d, 0
		ld	e, a
		ld	hl, subaco_line_buffer
		add	hl, de
		ld	e, (hl)
		inc	hl
		ld	d, (hl)
		ex	de, hl
		ld	d, 0
		ld	e, c
		add	hl, de
		pop de
		
		push bc
		ldi
		ldi
		ldi
		dec	l
		dec l
		dec	l
		inc	h
		ldi
		ldi
		ldi
		dec	l
		dec l
		dec	l
		inc	h
		ldi
		ldi
		ldi
		dec	l
		dec l
		dec	l
		inc	h
		ldi
		ldi
		ldi
		dec	l
		dec l
		dec	l
		inc	h
		ldi
		ldi
		ldi
		dec	l
		dec l
		dec	l
		inc	h
		ldi
		ldi
		ldi
		dec	l
		dec l
		dec	l
		inc	h
		ldi
		ldi
		ldi
		dec	l
		dec l
		dec	l
		inc	h
		ldi
		ldi
		ldi
		pop	bc
		
		push de
		ld	a, b
		inc a
		inc a
		add a, a
		ld	d, 0
		ld	e, a
		ld	hl, subaco_line_buffer
		add	hl, de
		ld	e, (hl)
		inc	hl
		ld	d, (hl)
		ex	de, hl
		ld	d, 0
		ld	e, c
		add	hl, de
		pop de
		
		push bc
		ldi
		ldi
		ldi
		dec	l
		dec l
		dec	l
		inc	h
		ldi
		ldi
		ldi
		dec	l
		dec l
		dec	l
		inc	h
		ldi
		ldi
		ldi
		dec	l
		dec l
		dec	l
		inc	h
		ldi
		ldi
		ldi
		dec	l
		dec l
		dec	l
		inc	h
		ldi
		ldi
		ldi
		dec	l
		dec l
		dec	l
		inc	h
		ldi
		ldi
		ldi
		dec	l
		dec l
		dec	l
		inc	h
		ldi
		ldi
		ldi
		dec	l
		dec l
		dec	l
		inc	h
		ldi
		ldi
		ldi
		pop	bc
		
		; Copy colours
		
		; hl = 22528 + y * 32 + x
		push de
		ld	h, 0
		ld	l, b
		add	hl, hl
		add	hl, hl
		add	hl, hl
		add	hl, hl
		add	hl, hl
		ld	d, 0
		ld	e, c
		add	hl, de
		ld	bc, 22528
		add	hl, bc
		pop	de
		
		ldi
		ldi
		ldi
		ld	bc, 29
		add	hl, bc
		ldi
		ldi
		ldi
		ld	bc, 29
		add	hl, bc
		ldi
		ldi
		ldi
		
		ret
		
	subaco_fill_buffers:
		ld	a, (sbc_y0)
		ld	b, a
		ld	a, (sbc_x0)
		ld	c, a
		ld	de, subaco_sprite_buffer_0
		call subaco_copy_to_buffer
		ld	a, (sbc_y1)
		ld	b, a
		ld	a, (sbc_x1)
		ld	c, a
		ld	de, subaco_sprite_buffer_1
		call subaco_copy_to_buffer
		ld	a, (sbc_y2)
		ld	b, a
		ld	a, (sbc_x2)
		ld	c, a
		ld	de, subaco_sprite_buffer_2
		call subaco_copy_to_buffer
		ret
		
	subaco_restore_background:
		ld	a, (sbccy0)
		ld	(sbc_y), a
		ld	a, (sbccx0)
		ld	(sbc_x), a
		ld	hl, subaco_sprite_buffer_0
		call subaco_draw_tile_in_hl
		ld	a, (sbccy1)
		ld	(sbc_y), a
		ld	a, (sbccx1)
		ld	(sbc_x), a
		ld	hl, subaco_sprite_buffer_1
		call subaco_draw_tile_in_hl
		ld	a, (sbccy2)
		ld	(sbc_y), a
		ld	a, (sbccx2)
		ld	(sbc_x), a
		ld	hl, subaco_sprite_buffer_2
		call subaco_draw_tile_in_hl
		ret
		
	subaco_draw_sprites:
		ld	a, (sbc_a0)
		and	a
		jr	z, sbc_gtsp1
		ld	a, (sbc_y0)
		ld	(sbc_y), a
		ld	a, (sbc_x0)
		ld	(sbc_x), a
		ld	hl, (sbc_n0)
		call subaco_draw_tile_in_hl
	sbc_gtsp1:
		ld	a, (sbc_a1)
		and	a
		jr	z, sbc_gtsp2
		ld	a, (sbc_y1)
		ld	(sbc_y), a
		ld	a, (sbc_x1)
		ld	(sbc_x), a
		ld	hl, (sbc_n1)
		call subaco_draw_tile_in_hl
	sbc_gtsp2:
		ld	a, (sbc_a2)
		and a
		jr	z, sbc_gtspe
		ld	a, (sbc_y2)
		ld	(sbc_y), a
		ld	a, (sbc_x2)
		ld	(sbc_x), a
		ld	hl, (sbc_n2)
		call subaco_draw_tile_in_hl
	sbc_gtspe:
		ret
	
	subaco_update_coordinates:
		ld	a, (sbc_y0)
		ld	(sbccy0), a	
		ld	a, (sbc_x0)
		ld	(sbccx0), a
		ld	a, (sbc_y1)
		ld	(sbccy1), a	
		ld	a, (sbc_x1)
		ld	(sbccx1), a
		ld	a, (sbc_y2)
		ld	(sbccy2), a	
		ld	a, (sbc_x2)
		ld	(sbccx2), a
		ret
		
End Asm
Asm
End Asm
End Sub
