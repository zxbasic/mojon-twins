' Simple inventory system
' For games such as SAIMAZOOM

' Copyleft 2012 The Mojon Twins

Const INVENTORYSIZE As uByte = 4
Const ITEMEMPTY As uByte = 255

Dim inventory (INVENTORYSIZE - 1) as uByte

' This sub initializes the inventory to INVENTORYSIZE empty spaces
Sub inventoryInit
	Dim i as uByte
	For i = 0 To INVENTORYSIZE - 1
		inventory (i) = ITEMEMPTY
	Next i
End Sub

' This function shuffles the inventory: an object enters, another
' exits (and is returned)
Function inventoryShuffle (itemIn as uByte)
	Dim itemOut as uByte
	itemOut = inventory (INVENTORYSIZE - 1)
	For i = INVENTORYSIZE - 1 To 1 Step -1
		inventory (i) = inventory (i - 1)
	Next i
	inventory (0) = itemIn
	Return itemOut
End Function

' This function returns item at position #i
Function inventoryRetrieve (i as uByte)
	Return inventory (i)
End Function
