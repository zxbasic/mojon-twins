Super Babalizoominator Construction Kit for ZX Basic
====================================================

This library was created with games like Saimazoom or Babaliba in mind, but it should be usable for anything.

It features: 

* Support for a tileset containing up to 256 24x24 pixel coloured tiles.
* Direct print any coloured tile on screen character coordinates.
* Up to three sprites which don't erase the background.

The sprite routines take like 1.5 of border time to execute. That's why we have included two versions of the update routine:

* The normal one (`subacoMoveSprites`) will run after a HALT. If you activate more than two sprites the third will blink when it's on the top five character lines.
* The delayed one (`subacoMoveSpritesLowBorder`) will run after a HALT and a loop to wait until the raster reaches the bottom border. There's no flickering this way but your game will run at 25fps at most. 25fps *SHOULD BE* enough.

I might write a tutorial if anybody is interested. Functions are self-explanatory. If you have worked with fourspriter this works in a very simmiliar fashion.

This library needs a tileset .bas file. Tilesets can be generated using the 24x24.exe program included alongside this library. The converter takes a 16x16 picture made of 24x24 tiles and will convert a number `n_tiles` of them:

```
$ 24x24.exe input.png output.bas n_tiles
```

This converter works as most of my converters. You can draw your picture approximating the speccy colours. The converter will detect hues; if any of RGB > 200, it will asume `BRIGHT 1`, `BRIGHT 0` otherwise.

Quick'n'dirty how to use this
-----------------------------

1. First draw your backdrop using `subacoDrawTile` calls. 
2. Then decide which sprites will be active using `subacoActivateSprite`.
3. Then assign a tile to each sprite. This tile can be changed at any time to animate them. Use `subacoDefSprite`
4. Then assign the coordinates to each sprite using `subacoMoveSprite`
5. Now call `subacoInit`.
6. Time for your game loop.
7. Update your coordinates, and then call `subacoMoveSprite` with them.
8. Call `subacoMoveSpritesLowBorder` to make it show.
9. Back to 6.
10. ??????
11. Profit.

Check `testlib.bas` for a very dumb demonstration. Also, the *Saimazoom-like Maritrini game* with the long title was created using ZX Basic and this library. Check it out.

Push requests to improve my dreadful assembly coding are most welcome. And appreciated.
