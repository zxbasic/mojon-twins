' 

#include once "tilestest.bas"
#include once "subacolib.bas"

Dim sx (2) as uByte
Dim sy (2) as uByte
Dim smx (2) as Byte
Dim smy (2) as Byte
Dim i as uByte
Dim x as uByte
Dim y as uByte

Border 0: Paper 0: Ink 7: Cls

' Draw a random backdrop

For y = 0 To 7
	For x = 0 To 9
		subacoDrawTile (1 + 3 * x, y * 3, 16 + Int (Rnd * 16))
	Next x
Next y

' Create coordinates, define sprites...

For i = 0 To 2
	sx (i) = int (rnd * 27) + 1
	sy (i) = int (rnd * 19) + 1
	smx (i) = (2 * int (rnd * 2)) - 1
	smy (i) = (2 * int (rnd * 2)) - 1
	
	subacoActivateSprite (i)
	subacoDefSprite (i, 2 * i)
	subacoMoveSprite (i, sx(i), sy(i))
Next i

' Init buffers

subacoInit ()

' Main loop

While Inkey$ = ""
	For i = 0 To 2
		sx (i) = sx (i) + smx (i)
		If sx (i) = 0 Or sx (i) = 29 Then smx (i) = -smx (i): End If
		sy (i) = sy (i) + smy (i)
		If sy (i) = 0 Or sy (i) = 21 Then smy (i) = -smy (i): End If
		
		' Animate
		subacoDefSprite (i, i + i + ((sx (i) >> 1) bAnd 1))
		
		' Move
		subacoMoveSprite (i, sx(i), sy (i))
	Next i

	' Make it show (no flicker, 25 fps)
	
	subacoMoveSpritesLowBorder ()
Wend
