Phantomas V 2
=============

*Unfinished* attempt to recreate an idea I had circa 1991 right after I coded my old game **Phantomas V** (check https://tcyr.wordpress.com/2007/10/16/grandes-juegos-de-lokosoft-10/ [in Spanish]). It was a direct sequel I couldn't produce at the time due to my limited skills.

It's almost done. It lacks the clock screen and some stuff.

Use O, P to move and SPACE to jump.

Uses **Fourspriter 2.1** to a good extent. Features 1991-style graphics ;). Compiles using Boriel's ZX Basic v. 1.2.7 r 2058 or newer.
