' Overlay.bas
' routines to draw the circularly lit overlay around Phantomas

Sub drawOverlay (x As Byte, y As Byte) 
	if x < 0 then x = 0: end if
	if y < 0 then y = 0: end if
	Poke @ovlCoords, x
	Poke 1 + @ovlCoords, y
	Asm
		call ovl_draw
	End Asm
End Sub

Sub delOverlay (x As Byte, y As Byte) 
	if x < 0 then x = 0: end if
	if y < 0 then y = 0: end if
	Poke @ovlCoords, x
	Poke 1 + @ovlCoords, y
	Asm
		call ovl_delete
	End Asm
End Sub

Sub overlayContainer ()
	Asm
		ovl_halo:	defb	0, 0, 1, 1, 1, 1, 0, 0
					defb 	0, 1, 5, 5, 5, 5, 1, 0
					defb	1, 5, 6, 6, 6, 6, 5, 1
					defb	1, 5, 6, 6, 6, 6, 5, 1
					defb	1, 5, 6, 6, 6, 6, 5, 1
					defb	1, 5, 6, 6, 6, 6, 5, 1
					defb 	0, 1, 5, 5, 5, 5, 1, 0
					defb	0, 0, 1, 1, 1, 1, 0, 0
	End Asm
	
ovlCoords:
	Asm
		ovl_x:		defb 	0
		ovl_y:		defb 	0
	End Asm
	
	Asm
		del_left:	; Draw 4 INK 0s @ ovl_x-1, ovl_y + 2
					; Calc address
					ld	a, (ovl_y)
					inc a
					inc a
					ld	h, 0
					ld	l, a
					add	hl, hl				; hl = ovl_y * 2
					add	hl, hl				; hl = ovl_y * 4
					add hl, hl				; hl = ovl_y * 8
					add	hl, hl				; hl = ovl_y * 16
					add	hl, hl				; hl = ovl_y * 32
					ld	a, (ovl_x)
					dec	a
					ld	d, 0
					ld	e, a
					add	hl, de				; hl = ovl_y * 32 + ovl_x
					ld	de, 22528
					add	hl, de				; hl = 22528 + ovl_y * 32 + ovl_x
					
					jr c4_vert
					
		del_right:	; Draw 4 INK 0s @ ovl_x+8, ovl_y + 2
					; Calc address
					ld	a, (ovl_y)
					inc a
					inc a
					ld	h, 0
					ld	l, a
					add	hl, hl				; hl = ovl_y * 2
					add	hl, hl				; hl = ovl_y * 4
					add hl, hl				; hl = ovl_y * 8
					add	hl, hl				; hl = ovl_y * 16
					add	hl, hl				; hl = ovl_y * 32
					ld	a, (ovl_x)
					add	a, 8
					ld	d, 0
					ld	e, a
					add	hl, de				; hl = ovl_y * 32 + ovl_x
					ld	de, 22528
					add	hl, de				; hl = 22528 + ovl_y * 32 + ovl_x
					
		; clear four, vertically
					
		c4_vert:	ld	bc, 32
					
					ld	a, (hl)
					and 248
					ld	(hl), a
					add hl, bc
					
					ld	a, (hl)
					and 248
					ld	(hl), a
					add hl, bc
					
					ld	a, (hl)
					and 248
					ld	(hl), a
					add hl, bc
					
					ld	a, (hl)
					and 248
					ld	(hl), a
					
					ret
		
		del_up:		; Draw 4 INK 0s @ ovl_x + 2, ovl_y-1
		
					; Calc address
					ld	a, (ovl_y)
					dec a
					ld	h, 0
					ld	l, a
					add	hl, hl				; hl = ovl_y * 2
					add	hl, hl				; hl = ovl_y * 4
					add hl, hl				; hl = ovl_y * 8
					add	hl, hl				; hl = ovl_y * 16
					add	hl, hl				; hl = ovl_y * 32
					ld	a, (ovl_x)
					inc	a
					inc	a
					ld	d, 0
					ld	e, a
					add	hl, de				; hl = ovl_y * 32 + ovl_x
					ld	de, 22528
					add	hl, de				; hl = 22528 + ovl_y * 32 + ovl_x
					
					jr c4_horz
		
		del_down:	; Draw 4 INK 0s @ ovl_x + 2, ovl_y + 8
		
					; Calc address
					ld	a, (ovl_y)
					add	a, 8
					ld	h, 0
					ld	l, a
					add	hl, hl				; hl = ovl_y * 2
					add	hl, hl				; hl = ovl_y * 4
					add hl, hl				; hl = ovl_y * 8
					add	hl, hl				; hl = ovl_y * 16
					add	hl, hl				; hl = ovl_y * 32
					ld	a, (ovl_x)
					inc	a
					inc	a
					ld	d, 0
					ld	e, a
					add	hl, de				; hl = ovl_y * 32 + ovl_x
					ld	de, 22528
					add	hl, de				; hl = 22528 + ovl_y * 32 + ovl_x
	
		c4_horz:	ld	a, (hl)
					and 248
					ld	(hl), a
					inc hl
					
					ld	a, (hl)
					and 248
					ld	(hl), a
					inc hl
					
					ld	a, (hl)
					and 248
					ld	(hl), a
					inc hl
					
					ld	a, (hl)
					and 248
					ld	(hl), a
					
					ret
					
		ovl_draw:		
					; First we calculate the address
					; address = 22528 + ovl_y * 32 + ovl_x
					
					ld 	a, (ovl_y)
					ld	h, 0
					ld	l, a
					add	hl, hl				; hl = ovl_y * 2
					add	hl, hl				; hl = ovl_y * 4
					add hl, hl				; hl = ovl_y * 8
					add	hl, hl				; hl = ovl_y * 16
					add	hl, hl				; hl = ovl_y * 32
					ld	a, (ovl_x)
					ld	d, 0
					ld	e, a
					add	hl, de				; hl = ovl_y * 32 + ovl_x
					ld	de, 22528
					add	hl, de				; hl = 22528 + ovl_y * 32 + ovl_x
					ld	de, ovl_halo				
					
					; Start the tint. 8x8 lines, unrolled.
					; Just change the INK attribute.
					
					; first line
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					inc	hl
					inc	de
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					inc	hl
					inc	de
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					inc	hl
					inc	de
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					inc	hl
					inc	de
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					inc	hl
					inc	de
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					inc	hl
					inc	de
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					inc	hl
					inc	de
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					ld 	bc, 25
					add hl, bc
					inc	de
					
					; second line
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					inc	hl
					inc	de
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					inc	hl
					inc	de
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					inc	hl
					inc	de
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					inc	hl
					inc	de
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					inc	hl
					inc	de
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					inc	hl
					inc	de
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					inc	hl
					inc	de
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					ld 	bc, 25
					add hl, bc
					inc	de
					
					; third line
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					inc	hl
					inc	de
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					inc	hl
					inc	de
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					inc	hl
					inc	de
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					inc	hl
					inc	de
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					inc	hl
					inc	de
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					inc	hl
					inc	de
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					inc	hl
					inc	de
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					ld 	bc, 25
					add hl, bc
					inc	de
					
					; fourth line
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					inc	hl
					inc	de
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					inc	hl
					inc	de
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					inc	hl
					inc	de
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					inc	hl
					inc	de
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					inc	hl
					inc	de
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					inc	hl
					inc	de
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					inc	hl
					inc	de
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					ld 	bc, 25
					add hl, bc
					inc	de
					
					; fifth line
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					inc	hl
					inc	de
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					inc	hl
					inc	de
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					inc	hl
					inc	de
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					inc	hl
					inc	de
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					inc	hl
					inc	de
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					inc	hl
					inc	de
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					inc	hl
					inc	de
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					ld 	bc, 25
					add hl, bc
					inc	de
					
					; sixth line
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					inc	hl
					inc	de
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					inc	hl
					inc	de
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					inc	hl
					inc	de
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					inc	hl
					inc	de
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					inc	hl
					inc	de
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					inc	hl
					inc	de
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					inc	hl
					inc	de
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					ld 	bc, 25
					add hl, bc
					inc	de
					
					; seventh line
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					inc	hl
					inc	de
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					inc	hl
					inc	de
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					inc	hl
					inc	de
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					inc	hl
					inc	de
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					inc	hl
					inc	de
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					inc	hl
					inc	de
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					inc	hl
					inc	de
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					ld 	bc, 25
					add hl, bc
					inc	de
					
					; eighth line
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					inc	hl
					inc	de
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					inc	hl
					inc	de
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					inc	hl
					inc	de
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					inc	hl
					inc	de
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					inc	hl
					inc	de
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					inc	hl
					inc	de
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					inc	hl
					inc	de
					
					ld	a, (de)
					ld	b, a
					ld	a, (hl)
					and	248
					or	b
					ld	(hl), a
					
					ret
		ovl_delete:		
					; First we calculate the address
					; address = 22528 + ovl_y * 32 + ovl_x
					
					ld 	a, (ovl_y)
					ld	h, 0
					ld	l, a
					add	hl, hl				; hl = ovl_y * 2
					add	hl, hl				; hl = ovl_y * 4
					add hl, hl				; hl = ovl_y * 8
					add	hl, hl				; hl = ovl_y * 16
					add	hl, hl				; hl = ovl_y * 32
					ld	a, (ovl_x)
					ld	d, 0
					ld	e, a
					add	hl, de				; hl = ovl_y * 32 + ovl_x
					ld	de, 22528
					add	hl, de				; hl = 22528 + ovl_y * 32 + ovl_x
					
					; Start the tint. 8x8 lines, unrolled.
					; Just change the INK to 0.
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					inc	hl
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					inc	hl
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					inc	hl
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					inc	hl
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					inc	hl
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					inc	hl
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					inc	hl
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					ld	bc, 25
					add	hl, bc
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					inc	hl
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					inc	hl
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					inc	hl
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					inc	hl
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					inc	hl
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					inc	hl
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					inc	hl
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					ld	bc, 25
					add	hl, bc
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					inc	hl
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					inc	hl
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					inc	hl
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					inc	hl
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					inc	hl
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					inc	hl
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					inc	hl
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					ld	bc, 25
					add	hl, bc
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					inc	hl
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					inc	hl
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					inc	hl
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					inc	hl
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					inc	hl
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					inc	hl
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					inc	hl
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					ld	bc, 25
					add	hl, bc
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					inc	hl
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					inc	hl
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					inc	hl
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					inc	hl
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					inc	hl
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					inc	hl
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					inc	hl
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					ld	bc, 25
					add	hl, bc
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					inc	hl
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					inc	hl
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					inc	hl
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					inc	hl
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					inc	hl
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					inc	hl
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					inc	hl
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					ld	bc, 25
					add	hl, bc
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					inc	hl
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					inc	hl
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					inc	hl
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					inc	hl
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					inc	hl
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					inc	hl
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					inc	hl
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					ld	bc, 25
					add	hl, bc
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					inc	hl
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					inc	hl
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					inc	hl
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					inc	hl
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					inc	hl
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					inc	hl
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					inc	hl
					
					ld	a, (hl)
					and	248
					ld	(hl), a
					ld	bc, 25
					add	hl, bc
					ret
	End Asm
End Sub
