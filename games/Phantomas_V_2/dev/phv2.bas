''
'' Phantomas V 2 by The Mojon Twins
'' A game for the LOKOsoft games that weren't 2011
''
'' Copyleft 2011 by the Mojon Twins
'' http://www.mojontwins.com
''
'' Compiles using Boriel's ZX Basic v. 1.2.7 r 2058
'' http://www.boriel.com/software/the-zx-basic-compiler/
''

#include once <asc.bas>

#include once "mapeador.bas"
#include once "fsp2.1.bas"
#include once "spriteset.bas"
#include once "overlay.bas"
#include once "bkd-forest.bas"
#include once "charset.bas"

' Constants (some)

Const MAXENERGY As uByte = 15

'' Global array definitions

' Player data

Dim plFacing As uByte						' player facing 0 = left, 32 = right
Dim plX As uByte							' player X
Dim plY As uByte							' player Y
Dim plSal As uByte							' player jumping, if set.
Dim plNu As uByte							' player jump counter, if plSal set.
Dim plStatus As uByte						' player status
Dim plFrame As uByte						' player animation frame
Dim plScore As uInteger						' player current score
Dim plLives As uInteger						' player current lives
Dim plKeys As uInteger
Dim plLastX As uByte						' to remember latest safe position
Dim plLastY As uByte
Dim plLastNPant As uByte
Dim plEnergy As uByte

' Brutihimo data

Dim brIsActive As uByte						' = 1 if brutihimo is active.
Dim brX As uByte							' brutihimo X
Dim brY As uByte							' brutihimo Y
Dim brSal As uByte							' brutihimo jumping, if set.
Dim brNu As uByte							' brutihimo jump counter, if brSal set.
Dim brStatus As uByte						' brutihimo status, 0 = idle, 1 = left, 2 = right, 3 = dying.
Dim brCounter As uByte						' brutihimo counter, measures # of game frames until status = 0
Dim brType As uByte							' 32 = normal, 48 = brujo.
Dim brFrame As uByte
Dim brFacing As uByte

' Lit data

Dim isLitArr (20) As uByte					' to remember which room is lit and which ain't.
Dim switchPostn (11) As uInteger => {530, 582, 955, 1105, 1375, 1439, 1724, 1939, 2200, 2351, 2605, 2749}

' Keys data

' **TODO**

'' Main program
'dim i as integer
'dim d as byte
'for i = 0 to 2939
'	d = peek (@mapData + i)
'	d = d - ((d>>4)<<4)
'	if d = 10 then print i:end if
'next i
'end

Poke uInteger 23606, @charset - 256
Border 0: Paper 0: Ink 0: Cls

fsp21SetGfxAddress (@spriteSet (0))			' Sets fourspriter to draw chars from suppaSpriteSet

theGame ()

Sub theGame ()
	Dim nPant As uByte
	Dim i As uByte
	Dim isLit As uByte
	Dim finished as uByte
	
	nPant = 20
	isLit = 1
	
	' turn off the lights in the castle
	For i = 0 To 20
		If (i Mod 3) = 2 Or i < 3 Then
			isLitArr (i) = 1
		Else
			isLitArr (i) = 0
		End If
	Next i
	
	' Turn off switches (in tiled map)
	For i = 0 To 11
		Poke (@mapData + switchPostn (i)), 122
	Next i
	
	' Deactivate sprites	
	For i = 1 To 3
		fsp21DeactivateSprite (i)
	Next i
	
	plX = 14
	plY = 16
	plSal = 0
	plNu = 0
	plStatus = 0
	plFrame = 0
	plScore = 0
	plLives = 5
	plKeys = 0
	plCX = plX
	plCY = plY
	plLastX = plX
	plLastY = plY
	plLastNPant = nPant
	plEnergy = MAXENERGY
	
	fsp21SetGfxSprite (0, 0, 1, 2, 3)
	fsp21ColourSprite (0, 71, 71, 71, 71)
	fsp21MoveSprite (0, 2 + plX, 2 + plY)
	fsp21DuplicateCoordinatesSprite (0)
	fsp21ActivateSprite (0)
	
	drawBackdrop (nPant)
	drawScreen (nPant, 1)
	
	fsp21InitSprites ()
	
	finished = 0
	While Not finished
		
		asm
			halt
			halt
		end asm
	
		playerMove (nPant)
		baddieMove (nPant)			' We always make the calculations even though nothing is shown, so the speed is more constant
		
		If isLit = 0 Then
			If plCX <> plX Or plCY <> plY Then
				delOverlay (plCX - 1, plCY - 1)
				drawOverlay (plX - 1, plY - 1)
			End If
		End If		
		
		fsp21UpdateSprites ()
		
		plCX = plX: plCY = plY
		
		' Touching a switch?
		
		If isEven (plX) And isEven (plY) And isLit = 0 Then
			If tiledScreenBehaviour(nPant, plX >> 1, plY >> 1) = 2 Then
				plStatus = 2
			End If
		End If
		
		' Check for death (the double if is for speed)
		
		If plStatus <> 0 Then
			If plStatus = 1 Then
				
				' Kill player
			
				plStatus = 0
				killAnimation ()
				
				If isLit = 0 Then
					fsp21BorraSprites ()
					delOverlay (plX - 1, plY - 1)
					fsp21InitSprites ()
				End If
				
				plX = plLastX
				plY = plLastY
				
				If plLastNPant <> nPant Then
					plCX = plX
					plCY = plY
					nPant = plLastNPant
					isLit = isLitArr (nPant)
					fsp21BorraSprites ()	
					drawBackdrop (nPant)
					drawScreen (nPant, isLit)
					createBaddieIfNecessary (nPant, isLit)
					fsp21InitSprites ()
					plCX = plX+1
				End If
				
				plLives = plLives - 1
				showStats ()
			ElseIf plStatus = 2 And isLit = 0 Then
			
				' Lit screen
				
				plStatus = 0
				isLit = 1
				isLitArr (nPant) = 1
				
				Beep .05, 0
				Beep .01, -10
				
				updateMap (nPant, plX >> 1, plY >> 1, 123)
				fsp21BorraSprites ()
				
				' Update map
				
				Asm
					call	lit_screen
				End Asm
				
				drawTile (plX + 2, plY + 2, 11)
				fsp21InitSprites ()
				fsp21UpdateSprites ()
				
				Beep .01,20
				Beep .1,30
				
			End If
		End If
		
		' Collisions
		
		If brIsActive = 1 Then
			If plX >= brX - 1 And plX <= brX + 1 And plY >= brY - 1 And plY <= brY + 1 Then
				plEnergy = plEnergy - 1
				showStats ()
				If plEnergy = 0 Then
					brIsActive = 0
					fsp21DeactivateSprite (1)
					killAnimation ()
					plLives = plLives - 1
					plEnergy = MAXENERGY
					showStats ()
				End If
			End If
		End If
		
		' Screen exit condition
		
		If (In (57342) = 253 Or In (57342) = 189) And plX = 0 Then
			plX = 26
			If nPant = 18 Then
				nPant = 99
			Else
				nPant = nPant - 1
			End If
			If nPant < 21 Then
				isLit = isLitArr (nPant)
			Else
				isLit = 1
			End If
			fsp21BorraSprites ()
			drawBackdrop (nPant)
			drawScreen (nPant, isLit)
			createBaddieIfNecessary (nPant, isLit)
			fsp21InitSprites ()
		ElseIf (In (57342) = 254 Or In (57342) = 190) And plX = 26 Then
			plX = 0
			If nPant < 99 Then
				nPant = nPant + 1
			Else
				nPant = 18
				plSal = 0
				plNu = 0
				plY = 16
			End If
			If nPant < 21 Then
				isLit = isLitArr (nPant)
			Else
				isLit = 1
			End If
			fsp21BorraSprites ()
			drawBackdrop (nPant)
			drawScreen (nPant, isLit)
			createBaddieIfNecessary (nPant, isLit)
			fsp21InitSprites ()
		ElseIf plY = 18 And plSal = 0 Then
			plY = 0
			nPant = nPant + 3
			isLit = isLitArr (nPant)
			fsp21BorraSprites ()
			drawBackdrop (nPant)
			drawScreen (nPant, isLit)
			createBaddieIfNecessary (nPant, isLit)
			fsp21InitSprites ()
		ElseIf plY = 0 And (plSal = 1 Or In (64510) = 254 Or In (64510) = 190) And nPant > 2 Then
			plY = 18
			plNu = plNu - 1
			nPant = nPant - 3
			isLit = isLitArr (nPant)
			fsp21BorraSprites ()
			drawBackdrop (nPant)
			drawScreen (nPant, isLit)
			createBaddieIfNecessary (nPant, isLit)
			fsp21InitSprites ()
		End If
	Wend
End Sub

Sub killAnimation ()
	Dim i As uByte

	For i = 1 To 10
		Beep .05, -10
		fsp21ColourSprite (0, 71, 71, 71, 71)
		fsp21UpdateSprites ()
		Beep .05, -20
		fsp21ColourSprite (0, 66, 66, 66, 66)
		fsp21UpdateSprites ()
	Next i
	fsp21SetGfxSprite (0, 12 + plFacing, 13 + plFacing, 14 + plFacing, 15 + plFacing)
	fsp21ColourSprite (0, 71, 71, 71, 71)
	fsp21UpdateSprites ()
	Beep 1, -22
End Sub

Sub showStats () 
	Print Bright 1; Ink 7; At 23, 0; "ENERGY "; plEnergy; " "
	Print Bright 1; Ink 7; At 23, 12; "LIVES "; plLives; " "
End Sub

Sub drawBackdrop (nPant As uByte)
	If (nPant Mod 3) <> 2 And nPant > 2 And nPant < 20 Then
		Cls
	Else
		Asm
			Call void_screen
			Call bkd_forest
		End Asm
	End If
	showStats ()
End Sub

Sub createBaddieIfNecessary (nPant As uByte, isLit As uByte)
	Dim xx As uByte
	Dim yy As uByte
	
	If isLit = 0 Or nPant = 20 Then 
		brIsActive = 0
		fsp21DeactivateSprite (1)	
	Else
		
		brIsActive = 1
	
		' Calculate which enemy
		
		brType = 32 + (Int (Rnd * 2) << 4)
	
		' Locate somewhere non-obstacled
		
		Do
			xx = Int (Rnd * 14)
			yy = Int (Rnd * 10)
		Loop While tiledScreenBehaviour (nPant, xx, yy) > 7
		
		brX = xx + xx
		brY = yy + yy
		
		' Initialize variables
		
		brSal = 0
		brNu = 0
		brFrame = 0
		brFacing = 0
		brStatus = 0
		
		' Create sprite
		
		fsp21SetGfxSprite (1, brType + brFacing, brType + brFacing + 1, brType + brFacing + 2, brType + brFacing + 3)
		If brType = 32 Then
			If brFacing = 0 Then
				fsp21ColourSprite (1, 68, 68, 66, 68)
			Else
				fsp21ColourSprite (1, 68, 68, 68, 66)
			End If
		Else
			fsp21ColourSprite (1, 66, 67, 67, 67)
		End If
		
		fsp21MoveSprite (1, 2 + brX, 2 + brY)
		fsp21DuplicateCoordinatesSprite (1)
		fsp21ActivateSprite (1)
		
	End If
End Sub

Sub playerMove (nPant As uByte)
	' Updates player based upon screen data 
	' and used input.
	
	Dim xx As uByte
	Dim yy As uByte
	Dim falling As uByte
		
	xx = plX >> 1: 	yy = plY >> 1
	
	' �Do we fall? Only if plSal = 0. Do we hit spikes in the process?
	
	falling = 0
	If plSal = 0 Then
		If isEven (plY) Then
			If tiledScreenBehaviour(nPant, xx, yy + 1) = 1 And _
				(isEven (plX) Or (Not isEven (plX) And _
				tiledScreenBehaviour(nPant, xx + 1, yy + 1) = 1)) Then
				plStatus = 1
			ElseIf tiledScreenBehaviour(nPant, xx, yy + 1) < 4 And _
				(isEven (plX) Or (Not isEven (plX) And _
				tiledScreenBehaviour(nPant, xx + 1, yy + 1) < 4)) Then
				plY = plY + 1
				falling = 1
			End If
		Else
			plY = plY + 1
			falling = 1
		End If
	End If
	
	' Move left/right
	
	yy = plY >> 1
	
	If In (57342) = 253 Or In (57342) = 189 Then
		If plFacing = 16 Then
			plFacing = 0
		Else
			plFrame = plFrame + 1: If plFrame = 4 Then plFrame = 0: End If
			If plX > 0 Then
				If isEven (plX) Then
					If tiledScreenBehaviour(nPant, xx - 1, yy) < 8 And _
						(isEven (plY) Or (Not isEven (plY) And _
						tiledScreenBehaviour(nPant, xx - 1, yy + 1) < 8)) Then
						plX = plX - 1
					End If
				Else
					plX = plX - 1
				End If
			End If
		End If
	End If
	
	If In (57342) = 254 Or In (57342) = 190 Then
		If plFacing = 0 Then
			plFacing = 16
		Else
			plFrame = plFrame + 1: If plFrame = 4 Then plFrame = 0: End If
			If plX < 28 Then
				If isEven (plX) Then
					If tiledScreenBehaviour(nPant, xx + 1, yy) < 8 And _
						(isEven (plY) Or (Not isEven (plY) And _
						tiledScreenBehaviour(nPant, xx + 1, yy + 1) < 8)) Then
						plX = plX + 1
					End If
				Else
					plX = plX + 1
				End If
			End If
		End If
	End If
	
	' Jump
	
	xx = plX >> 1
	
	If falling = 0 And plSal = 0 Then
		If In (32766) = 254 Or In (32766) = 190 Then
			If tiledScreenBehaviour(nPant, xx, yy + 1) > 3 Or _
				(Not isEven (plX) And tiledScreenBehaviour(nPant, xx + 1, yy + 1) > 3) Then
				plSal = 1
				plNu = 0
				plLastX = plX
				plLastY = plY
				plLastNPant = nPant
			End If
		End If
	End if
	
	If plSal = 1 Then
		If plY > 0 Then
			If isEven (plY) Then
				If tiledScreenBehaviour(nPant, xx, yy - 1) < 8 And _
					(isEven (plX) Or (Not isEven (plX) And _
					tiledScreenBehaviour(nPant, xx + 1, yy - 1) < 8)) Then
					plY = plY - 1
				End If
			Else
				plY = plY - 1
			End If
		End If
		plNu = plNu + 1: If plNu = 6 Then plSal = 0: End If
	End If
	
	' Animate
	
	If plSal = 0 And falling = 0 Then
		If isEven(plFrame) Then
			fsp21SetGfxSprite (0, 0 + plFacing, 1 + plFacing, 2 + plFacing, 3 + plFacing)
		Else
			fsp21SetGfxSprite (0, 4 + plFacing, 5 + plFacing, 6 + plFacing, 7 + plFacing)
		End If
	Else
		fsp21SetGfxSprite (0, 8 + plFacing, 9 + plFacing, 10 + plFacing, 11 + plFacing)
	End If
	
	' Update fourspriter
	
	fsp21MoveSprite (0, 2 + plX, 2 + plY)					' Moves the sprite to new location.
	
End Sub

Sub baddieMove (nPant As uByte)
	' Updates baddie based upon screen data 
	' and exciting randomness

	Dim xx As uByte
	Dim yy As uByte
	Dim falling As uByte
	Dim rd As uByte
	Dim frame As uByte
	Dim count As uByte
		
	xx = brX >> 1: 	yy = brY >> 1

	If brType = 32 Then
		falling = 0
		If brSal = 0 Then
			If isEven (brY) Then
				If tiledScreenBehaviour(nPant, xx, yy + 1) < 1 And _
					(isEven (brX) Or (Not isEven (brX) And _
					tiledScreenBehaviour(nPant, xx + 1, yy + 1) < 1)) Then
					brY = brY + 1
					falling = 1
				End If
				If brY = 18 Then
					brY = 0
					count = 0
					Do
						xx = Int (Rnd * 14)
						count = count + 1
					Loop While tiledScreenBehaviour (nPant, xx, 0) > 7 and count < 10
					brX = xx + xx
				End If
			Else
				brY = brY + 1
				falling = 1
			End If
		End If
		yy = brY >> 1
		If falling = 0 Then
			If brStatus = 1 Then
				brFrame = brFrame + 1: If brFrame = 2 Then brFrame = 0: End If
				brFacing = 0
				If brX > 0 Then
					If isEven (brX) Then
						If tiledScreenBehaviour(nPant, xx - 1, yy) < 8 And _
							(isEven (brY) Or (Not isEven (brY) And _
							tiledScreenBehaviour(nPant, xx - 1, yy + 1) < 8)) Then
							brX = brX - 1
						End If
					Else
						brX = brX - 1
					End If
				End If
			ElseIf brStatus = 2 Then
				brFrame = brFrame + 1: If brFrame = 2 Then brFrame = 0: End If
				brFacing = 8
				If brX < 28 Then
					If isEven (brX) Then
						If tiledScreenBehaviour(nPant, xx + 1, yy) < 8 And _
							(isEven (brY) Or (Not isEven (brY) And _
							tiledScreenBehaviour(nPant, xx + 1, yy + 1) < 8)) Then
							brX = brX + 1
						End If
					Else
						brX = brX + 1
					End If
				End If
			ElseIf brStatus = 0 Then
				brCounter = 2 + Int (Rnd * 8)
				rd = Int (Rnd * 10)
				brStatus = 0
				If rd = 4 Or rd = 5 Then 
					brStatus = 2
				End If
				If rd = 6 Or rd = 7 Then
					brStatus = 1
				End If
			End If
			brCounter = brCounter - 1
			If brCounter = 0 Then 
				brStatus = 0
			End If
		End If
		xx = brX >> 1
		
		If falling = 0 And brSal = 0 Then
			rd = Int (Rnd * 16)
			If rd = 8 Then
				If tiledScreenBehaviour(nPant, xx, yy + 1) > 3 Or _
					(Not isEven (brX) And tiledScreenBehaviour(nPant, xx + 1, yy + 1) > 3) Then
					brSal = 1
					brNu = 0
				End If
			End If
		End if
		
		If brSal = 1 Then
			If brY > 0 Then
				If isEven (brY) Then
					If tiledScreenBehaviour(nPant, xx, yy - 1) < 8 And _
						(isEven (brX) Or (Not isEven (brX) And _
						tiledScreenBehaviour(nPant, xx + 1, yy - 1) < 8)) Then
						brY = brY - 1
					End If
				Else
					brY = brY - 1
				End If
			End If
			brNu = brNu + 1: If brNu = 6 Then brSal = 0: End If
		End If
		
		If brFacing = 0 Then
			fsp21ColourSprite (1, 68, 68, 66, 68)
		Else
			fsp21ColourSprite (1, 68, 68, 68, 66)
		End If
	Else
		rd = Int (Rnd * 4)
		If rd = 1 Then
			If plX > brX Then
				brFrame = brFrame + 1: If brFrame = 2 Then brFrame = 0: End If
				brFacing = 8
				If brX < 28 Then
					If isEven (brX) Then
						If tiledScreenBehaviour(nPant, xx + 1, yy) < 8 And _
							(isEven (brY) Or (Not isEven (brY) And _
							tiledScreenBehaviour(nPant, xx + 1, yy + 1) < 8)) Then
							brX = brX + 1
						End If
					Else
						brX = brX + 1
					End If
				End If
			ElseIf plX < brX Then
				brFrame = brFrame + 1: If brFrame = 2 Then brFrame = 0: End If
				brFacing = 0
				If brX > 0 Then
					If isEven (brX) Then
						If tiledScreenBehaviour(nPant, xx - 1, yy) < 8 And _
							(isEven (brY) Or (Not isEven (brY) And _
							tiledScreenBehaviour(nPant, xx - 1, yy + 1) < 8)) Then
							brX = brX - 1
						End If
					Else
						brX = brX - 1
					End If
				End If
			End If		
		End If
		
		rd = Int (Rnd * 4)
		If rd = 1 Then
			xx = brX >> 1
			If plY > brY Then	
				If brY < 18 Then
					If isEven (brY) Then
						If tiledScreenBehaviour(nPant, xx, yy + 1) < 8 And _
							(isEven (brX) Or (Not isEven (brX) And _
							tiledScreenBehaviour(nPant, xx + 1, yy + 1) < 8)) Then
							brY = brY + 1
						End If
					Else
						brY = brY + 1
					End If
				End If
			ElseIf plY < brY Then			
				If brY > 0 Then
					If isEven (brY) Then
						If tiledScreenBehaviour(nPant, xx, yy - 1) < 8 And _
							(isEven (brX) Or (Not isEven (brX) And _
							tiledScreenBehaviour(nPant, xx + 1, yy - 1) < 8)) Then
							brY = brY - 1
						End If
					Else
						brY = brY - 1
					End If
				End If
			End If
		End If
	End If
	
	' Select frame
	
	frame = brType + brFacing + (brFrame << 2)
	fsp21SetGfxSprite (1, frame, frame + 1, frame + 2, frame + 3)
	
	' Update fourspriter
	
	fsp21MoveSprite (1, 2 + brX, 2 + brY)					' Moves the sprite to new location.
End Sub

Function isEven (n As uByte) As uByte
	Return ((n>>1)<<1) = n
End Function
