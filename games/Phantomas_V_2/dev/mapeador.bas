' Mapeador.bas
' Incluye el tileset y las rutinas para dibujar las pantallas
' contenidas en el array mapa.

' tilesBehaviour

Dim tilesBehaviour (15) as uByte => {8, 8, 8, 8, 8, 8, 0, 0, 8, 8, 2, 0, 0, 1, 8, 8}


' This sub draws the screen # nPant in the dark (lit = 0) or lit (lit = 1)

Sub drawScreen (nPant As Byte, lit As Byte)
	Dim realN As Byte
	
	If nPant > 20 Then realN = nPant: nPant = 21: End If	

	Poke @dataPool, nPant
	Poke @dataPool + 1, lit
	
	If lit = 0 Then
		Asm
			call	void_screen
		End Asm
	End If
		
	Asm
		call	draw_screen
	End Asm
	
	If lit <> 0 Then
		Asm
			call	lit_screen
		End Asm
	End If
	
	If nPant > 20 Then Print Over 2; Inverse 1; Ink 6; At 17, 18; realN;: End If
End Sub


' This sub draws a single tile

Sub drawTile (x As Byte, y As Byte, t as Byte)
	Poke @dataPool + 2, x
	Poke @dataPool + 3, y 
	Poke @dataPool + 8, t
	Asm
		call	draw_tile
	End Asm
End Sub


' This sub checks for what's at tile x, y.
' returns 255 if out of screen

Function whatAt (nPant As Integer, x As Byte, y As Byte) As UByte
	If x < 0 Or x > 13 Or y < 0 Or y > 9 Then 
		Return 255
	End If
	
	' nPant * 140 + 14 * y + x = (nPant * 128 + nPant * 8 + nPant * 4) + (y * 8 + y * 4 + y * 2) + x
	
	Return Peek (@mapData + (nPant << 7) + (nPant << 3) + (nPant << 2) + (y << 3) + (y << 2) + y + y + x)
End Function


' This sub writes a tile to the map 

Sub updateMap (nPant As Integer, x As Byte, y As Byte, tile As Byte)
	Poke @mapData + (nPant << 7) + (nPant << 3) + (nPant << 2) + (y << 3) + (y << 2) + y + y + x, tile
End Sub


' This sub tells about tile behaviour

Function tiledScreenBehaviour (nPant As Integer, x As Byte, y As Byte) As UByte
	Dim what As UByte
	Dim res As UByte
	
	If nPant > 20 Then nPant = 21: End If	
	what = whatAt (nPant, x, y)
	
	If what < 16 Then	
		res = 0
	Else 
		res = tilesBehaviour (what - ((what >> 4) << 4))
	End If
	
	Return res
End Function


Sub dsContainer ()

mapaCharset:
Asm
	map_charset:	defb	  0,  0,  0,  0,  0,  0,  0,  0
					defb	  0, 21, 63, 87, 63, 87, 63,125
					defb	  0, 21,191,117,255,245,255,253
					defb	 85,170, 85,170, 85,170, 85,170
					defb	  1, 15,  5,127, 27,127,111,255
					defb	128,240,160,254,216,254,246,255
					defb	 56,124,254,255,223,175,213,126
					defb	  8, 12, 30, 62, 62, 62, 28,  0
					defb	  0,  8, 66,  0, 60,126,126,126
					defb	  0,  0,  0,  0, 16,129, 88,126
					defb	170, 86, 46, 30, 46, 86,170,  0
					defb	  0,  0,  0,  0,  0,  0,  0,  0
					defb	  0,  0,  0,  0,  0,  0,  0,  0
					defb	  0,  0,  0,  0,  0,  0,  0,  0
					defb	  0,  0,  0,  0,  0,  0,  0,  0
					defb	  0,  0,  0,  0,  0,  0,  0,  0
					defb	  0,  0,  0,  0,  0,  0,  0,  0
					defb	  0,  0,  0,  0,  0,  0,  0,  0
					defb	  0,  0,  0,  0,  0,  0,  0,  0
					defb	  0,  0,  0,  0,  0,  0,  0,  0
					defb	  0,  0,  0,  0,  0,  0,  0,  0
					defb	  0,  0,  0,  0,  0,  0,  0,  0
					defb	  0,  0,  0,  0,  0,  0,  0,  0
					defb	  0,  0,  0,  0,  0,  0,  0,  0
					defb	  0,  0,  0,  0,  0,  0,  0,  0
					defb	  0,  0,  0,  0,  0,  0,  0,  0
					defb	  0,  0,  0,  0,  0,  0,  0,  0
					defb	  0,  0,  0,  0,  0,  0,  0,  0
					defb	  0,  0,  0,  0,  0,  0,  0,  0
					defb	  0,  0,  0,  0,  0,  0,  0,  0
					defb	  0,  0,  0,  0,  0,  0,  0,  0
					defb	  0,  0,  0,  0,  0,  0,  0,  0
					defb	  0,  1, 63, 95,111, 95,191,223
					defb	  0,254,255,254,213,171,223,239
					defb	190,223,231,239,247,106, 53, 15
					defb	215, 15,215,232,223,191,127,224
					defb	  0,127,255,191, 31,191,189,187
					defb	  0,252,214,171,247,255,240,238
					defb	127,111, 87,235,127,190,223,  0
					defb	223,239,239,239,222, 60,240,  0
					defb	 30,111,115,125,126,221,170,247
					defb	  0,240,252,254,254,126,238,208
					defb	255,255, 63, 95, 63, 95, 53, 10
					defb	190,222,254,254,246,170, 92,192
					defb	  0, 31, 31,108,115, 62, 61,122
					defb	  0,128,248,124,190,220, 94,190
					defb	125,251,124, 63, 63,126, 63, 15
					defb	190,127,246,234,116,168, 80,192
					defb	 15, 53,106, 95,191,223, 63, 95
					defb	248,254,189, 91,247,247,239,223
					defb	 63, 95, 61, 94, 63, 94, 61, 95
					defb	223,223,223,191,127,255,255,255
					defb	 61, 95, 61, 95, 63,127,255,223
					defb	231,219,181,187,255,191,190,191
					defb	125,251,124, 63, 63, 94, 53, 11
					defb	190,127,246,234,116,168, 80,192
					defb	  0,  0,  0,  0, 48, 73, 37,  5
					defb	  0, 96,144,128,128,  0,  8, 20
					defb	  3,  3, 67,167, 55, 31, 31, 15
					defb	  4,  4,136,240,224,196,202,240
					defb	 85,  8,  0,136, 85,  0,  0,  8
					defb	 85,  8,  0,136, 85,128,  0,136
					defb	 85, 10,  5,138, 85,  0,  0,  8
					defb	 85,136, 64,168, 85,128,  0,136
					defb	255,255,  0,127,127,  0, 93, 77
					defb	255,255,  0,254,254,  0,186,178
					defb	 33, 25, 43, 40, 11, 44, 13,  6
					defb	132,152,212, 20,208, 52,176,110
					defb	 19, 80, 85,125,120, 51,  6,  7
					defb	206, 22,246,118,  6,230,102,228
					defb	  0, 30,112,127, 63,  0,255,255
					defb	 14,238, 14,202, 16,  0,255,255
					defb	 15,111,111, 96,111,111,111,111
					defb	240,246,246,  6,246, 22,214,214
					defb	111,103,107,101, 96,127,127,  0
					defb	246,246,246,102,  6,254,254,  0
					defb	  0,127,127, 96,101,107,103,111
					defb	  0,254,254,  6,102,246,246,246
					defb	111,111,111,111, 96,111,111, 15
					defb	214,214, 22,246,  6,246,246,240
					defb	  0, 31, 63,112, 96,112, 63, 31
					defb	  0,248,252, 14,  6, 14,252,248
					defb	  1,  1,  1,  1,  1,  1,  1,  0
					defb	128,188,104,188, 96,184,188,  0
					defb	  0,  8,  8,  8, 24, 24, 24, 24
					defb	  0,  0,  8,  8,  8, 24, 24, 24
					defb	 24, 24, 56, 60, 60,124,126,254
					defb	 24, 24, 56, 56, 56, 60,126,254
					defb	170, 85,219,109,221,119,222,247
					defb	170, 85,219,109,221,119,222,247
					defb	125,239,255,255,255,255,255,255
					defb	125,239,255,255,255,255,255,255
					defb	255,191,252,248,240,240,248,252
					defb	254,250,126, 62, 30, 30, 62,126
					defb	254,252,252,248,248,191,255,  0
					defb	254,126,126, 62, 62,250,254,  0
End Asm

' Map data. Contains the tiles used in each screen

mapData:
Asm
    map_data:		defb 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
					defb 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 104, 0, 0, 104, 0, 0, 104, 0, 0, 0, 0, 0, 86, 0, 105, 0, 86, 105, 86, 0, 105, 0, 0, 0, 78, 78, 78, 78, 78, 78, 78, 78, 78, 78, 78, 78, 78, 78, 113, 112, 113, 112, 113, 112, 113, 112, 113, 112, 113, 112, 113, 112
					defb 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 96, 97, 46, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 96, 97, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 96, 97, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 100, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 86, 101, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 96, 97, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 100, 0, 0, 0, 0, 0, 0, 0, 0, 86, 0, 0, 0, 86, 101, 78, 78, 78, 0, 0, 0, 0, 0, 98, 0, 0, 0, 99, 96, 113, 112, 113, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 99
					
					defb 0, 0, 64, 65, 64, 65, 0, 64, 65, 64, 65, 64, 65, 80, 0, 64, 65, 0, 0, 64, 65, 0, 0, 0, 0, 0, 80, 81, 64, 65, 0, 0, 23, 0, 0, 0, 23, 23, 23, 23, 0, 0, 51, 64, 65, 0, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 64, 65, 0, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 70, 68, 64, 65, 109, 120, 109, 125, 80, 81, 80, 81, 0, 23, 82, 69, 0, 80, 81, 121, 80, 81, 0, 80, 81, 0, 80, 81, 80, 68, 80, 81, 80, 81, 0, 0, 0, 0, 0, 0, 0, 122, 0, 69, 0, 80, 81, 0, 0, 23, 23, 23, 23, 23, 23, 70, 0, 51, 80, 81, 0, 0, 23, 23, 23, 23, 23, 23, 80, 81, 0
					defb 81, 80, 81, 80, 81, 80, 81, 80, 81, 0, 80, 81, 80, 81, 83, 0, 0, 0, 0, 0, 0, 0, 122, 80, 81, 0, 0, 80, 0, 0, 23, 23, 23, 23, 23, 23, 23, 0, 0, 0, 23, 0, 23, 23, 23, 52, 0, 23, 70, 23, 23, 23, 70, 23, 23, 23, 23, 23, 52, 53, 0, 23, 82, 0, 23, 23, 83, 0, 23, 23, 80, 81, 53, 52, 0, 23, 0, 0, 23, 23, 0, 0, 23, 66, 81, 0, 0, 53, 109, 125, 125, 125, 0, 23, 23, 23, 23, 0, 0, 0, 52, 80, 81, 80, 81, 83, 0, 23, 67, 0, 23, 23, 23, 23, 53, 52, 0, 0, 0, 0, 0, 23, 0, 0, 23, 70, 23, 23, 0, 53, 0, 23, 23, 23, 23, 23, 23, 23, 23, 48
					defb 0, 80, 81, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 100, 81, 23, 23, 83, 0, 0, 0, 0, 0, 0, 98, 0, 0, 101, 23, 23, 23, 83, 0, 0, 0, 86, 0, 0, 0, 0, 0, 100, 23, 66, 80, 81, 0, 0, 0, 99, 0, 0, 0, 0, 86, 101, 23, 80, 81, 0, 0, 0, 0, 0, 0, 0, 86, 0, 96, 97, 80, 81, 80, 81, 0, 0, 0, 0, 0, 0, 99, 0, 0, 100, 23, 80, 81, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 101, 23, 80, 81, 0, 0, 0, 0, 0, 0, 0, 0, 98, 0, 100, 23, 120, 80, 81, 0, 0, 0, 0, 0, 0, 0, 0, 0, 101, 49, 105, 81, 0, 0, 0, 0, 0, 0, 0, 0, 0, 96, 97
					
					defb 80, 81, 0, 0, 23, 23, 23, 23, 23, 23, 23, 48, 49, 0, 50, 0, 23, 23, 23, 23, 23, 23, 48, 49, 48, 49, 0, 48, 51, 0, 23, 23, 23, 51, 23	, 23, 0, 32, 33, 48, 49, 51, 84, 0, 23, 23, 51, 23, 23, 23, 23, 0, 0, 0, 0, 0, 85, 51, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 82, 0, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 84, 0, 23, 23, 120, 0, 23, 23, 23, 70, 23, 23, 23, 23, 85, 84, 0, 23, 121, 51, 23, 23, 23, 51, 23, 23, 23, 23, 0, 85, 0, 122, 52, 109, 125, 125, 125, 125, 48, 49, 0, 23, 0, 48, 49, 51, 53, 48, 49, 50, 51, 32, 33, 48, 49, 0
					defb 0, 0, 48, 49, 0, 23, 23, 23, 23, 23, 23, 23, 23, 52, 49, 51, 70, 52, 32, 33, 0, 23, 48, 49, 109, 48, 49, 53, 51, 48, 49, 53, 70, 48, 49, 0, 0, 48, 49, 50, 32, 33, 0, 0, 0, 48, 49, 0, 0, 0, 23, 0, 52, 32, 33, 50, 23, 23, 23, 0, 0, 0, 23, 51, 0, 23, 53, 0, 48, 49, 23, 23, 23, 23, 23, 23, 23, 0, 70, 23, 0, 0, 50, 32, 23, 23, 23, 23, 23, 23, 23, 23, 51, 0, 23, 23, 36, 51, 23, 34, 0, 23, 23, 23, 125, 32, 33, 109, 125, 125, 37, 0, 23, 0, 32, 33, 0, 23, 35, 0, 0, 32, 33, 32, 33, 122, 23, 23, 0, 0, 0, 23, 0, 0, 23, 0, 0, 0, 0, 0
					defb 48, 49, 84, 0, 0, 0, 0, 0, 0, 0, 0, 0, 100, 99, 70, 36, 85, 0, 0, 0, 0, 0, 0, 0, 0, 86, 101, 100, 51, 37, 82, 0, 0, 0, 0, 0, 0, 0, 0, 96, 97, 101, 48, 49, 84, 0, 0, 0, 0, 0, 0, 0, 0, 0, 100, 98, 48, 49, 85, 0, 0, 0, 0, 0, 98, 0, 0, 0, 101, 100, 33, 70, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 101, 48, 49, 51, 0, 0, 0, 0, 0, 0, 98, 0, 0, 0, 100, 23, 48, 49, 0, 0, 0, 0, 0, 0, 0, 86, 0, 0, 101, 23, 23, 0, 0, 0, 0, 0, 0, 0, 0, 99, 0, 96, 97, 23, 23, 0, 0, 0, 0, 0, 0, 0, 0, 0, 96, 97, 0
					
					defb 0, 84, 48, 49, 48, 49, 32, 33, 48, 49, 0, 48, 49, 0, 84, 85, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 32, 33, 85, 0, 0, 23, 23, 23, 23, 23, 23, 23, 23, 23, 0, 48, 82, 84, 0, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 51, 84, 85, 84, 109, 125, 125, 52, 109, 125, 125, 51, 23, 23, 0, 85, 82, 85, 80, 81, 83, 53, 80, 81, 80, 81, 0, 23, 23, 0, 84, 80, 81, 0, 0, 0, 0, 0, 0, 0, 0, 23, 50, 84, 85, 84, 0, 0, 23, 23, 23, 23, 23, 23, 23, 23, 0, 85, 0, 85, 122, 23, 23, 23, 23, 23, 23, 23, 23, 120, 0, 82, 78, 78, 78, 78, 125, 125, 125, 125, 78, 78, 78, 121, 78
					defb 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 35, 0, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 52, 0, 49, 48, 49, 0, 23, 23, 23, 23, 23, 23, 23, 122, 53, 0, 48, 49, 0, 0, 50, 51, 0, 23, 23, 23, 48, 49, 51, 0, 0, 0, 0, 23, 0, 0, 0, 23, 23, 23, 0, 0, 0, 0, 23, 23, 50, 0, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 51, 0, 0, 0, 70, 23, 23, 23, 70, 23, 23, 23, 70, 23, 0, 0, 23, 23, 120, 0, 23, 23, 120, 0, 23, 23, 120, 0, 23, 23, 70, 23, 121, 0, 23, 23, 121, 0, 70, 23, 121, 0, 78, 78, 78, 78, 78, 109, 125, 125, 78, 78, 78, 78, 78, 78
					defb 23, 23, 0, 0, 0, 0, 0, 86, 0, 0, 0, 0, 96, 97, 23, 23, 0, 0, 0, 0, 0, 99, 0, 0, 0, 96, 97, 0, 23, 23, 0, 0, 0, 0, 0, 0, 0, 86, 0, 0, 98, 0, 23, 23, 0, 0, 0, 0, 0, 0, 0, 96, 86, 98, 100, 0, 23, 23, 0, 0, 0, 0, 86, 0, 0, 0, 96, 97, 101, 0, 23, 23, 0, 0, 0, 96, 97, 0, 0, 86, 0, 0, 100, 86, 23, 23, 0, 0, 98, 0, 0, 0, 0, 98, 0, 86, 101, 100, 23, 23, 0, 0, 0, 0, 0, 0, 0, 0, 86, 96, 97, 101, 23, 23, 0, 98, 0, 0, 0, 0, 0, 96, 97, 0, 96, 97, 78, 78, 78, 84, 0, 0, 0, 0, 0, 0, 0, 0, 0, 99
					
					defb 84, 80, 81, 80, 81, 80, 81, 80, 81, 80, 81, 66, 80, 81, 85, 66, 0, 0, 80, 81, 0, 0, 0, 0, 0, 0, 0, 0, 80, 81, 0, 23, 0, 0, 0, 23, 23, 23, 23, 23, 23, 23, 84, 0, 122, 23, 23, 23, 66, 0, 23, 80, 81, 0, 80, 81, 85, 0, 23, 23, 23, 66, 0, 23, 23, 0, 0, 0, 0, 0, 82, 0, 23, 120, 0, 0, 0, 23, 23, 23, 23, 23, 23, 23, 120, 0, 66, 121, 66, 0, 23, 23, 120, 0, 23, 64, 65, 0, 121, 0, 0, 66, 0, 0, 23, 23, 121, 0, 23, 65, 67, 0, 66, 0, 23, 0, 0, 23, 83, 64, 65, 0, 23, 0, 0, 64, 0, 66, 0, 23, 23, 83, 64, 65, 109, 125, 125, 125, 125, 0
					defb 80, 81, 67, 67, 80, 81, 67, 80, 81, 80, 81, 78, 78, 80, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 80, 81, 80, 81, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 0, 0, 80, 81, 80, 81, 0, 23, 80, 81, 0, 23, 23, 23, 23, 23, 0, 80, 0, 0, 0, 23, 0, 80, 81, 0, 23, 23, 23, 23, 23, 0, 23, 23, 23, 23, 23, 0, 80, 81, 0, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 0, 80, 81, 0, 23, 23, 23, 23, 23, 23, 64, 65, 0, 23, 80, 81, 0, 23, 23, 23, 23, 23, 65, 64, 65, 82, 0, 23, 67, 122, 0, 23, 64, 65, 0, 23, 0, 0, 0, 0, 109, 125, 64, 65, 64, 65, 0, 64, 65, 0
					defb 81, 80, 81, 85, 0, 0, 0, 0, 86, 0, 0, 0, 0, 100, 66, 80, 81, 0, 0, 0, 0, 0, 96, 97, 0, 0, 86, 101, 80, 81, 80, 81, 0, 0, 0, 0, 0, 0, 0, 0, 96, 97, 81, 23, 0, 0, 0, 0, 0, 0, 0, 86, 0, 0, 0, 100, 23, 23, 0, 0, 0, 0, 0, 0, 0, 96, 97, 0, 0, 101, 23, 23, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 86, 98, 23, 23, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 96, 97, 23, 23, 0, 0, 0, 0, 0, 0, 0, 0, 86, 0, 0, 99, 23, 84, 66, 0, 0, 0, 0, 0, 0, 96, 97, 0, 0, 100, 23, 85, 84, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 101
					
					defb 0, 83, 0, 23, 23, 83, 64, 65, 64, 65, 0, 64, 65, 0, 83, 64, 65, 0, 23, 0, 83, 0, 0, 82, 64, 65, 0, 0, 64, 65, 0, 0, 23, 23, 0, 0, 23, 0, 0, 0, 0, 23, 64, 65, 0, 83, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 0, 64, 65, 0, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 64, 65, 0, 0, 23, 83, 0, 83, 0, 23, 23, 82, 0, 80, 120, 0, 23, 23, 23, 0, 82, 0, 0, 23, 23, 0, 83, 0, 121, 120, 122, 23, 23, 23, 0, 0, 23, 23, 23, 23, 0, 0, 120, 121, 83, 0, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 121, 109, 125, 125, 125, 83, 109, 125, 125, 125, 125, 125, 125, 125
					defb 0, 82, 0, 64, 65, 64, 65, 83, 64, 65, 64, 65, 84, 0, 0, 67, 64, 65, 0, 83, 0, 64, 65, 0, 83, 0, 85, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 82, 0, 23, 23, 23, 23, 23, 23, 23, 81, 0, 23, 23, 83, 0, 83, 0, 23, 23, 83, 0, 23, 83, 0, 0, 23, 23, 0, 0, 0, 0, 23, 82, 0, 82, 0, 23, 23, 67, 0, 23, 23, 23, 23, 23, 23, 0, 0, 0, 0, 122, 23, 0, 0, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 84, 125, 125, 125, 125, 125, 125, 125, 125, 125, 125, 125, 125, 82, 85
					defb 23, 83, 85, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 99, 23, 23, 83, 0, 0, 0, 0, 0, 0, 0, 0, 86, 99, 100, 23, 23, 0, 0, 0, 0, 0, 0, 0, 0, 0, 96, 97, 101, 23, 23, 0, 0, 0, 0, 0, 98, 0, 0, 0, 0, 96, 97, 23, 23, 0, 80, 81, 0, 0, 0, 0, 0, 0, 0, 0, 100, 80, 81, 83, 84, 0, 98, 0, 0, 0, 98, 0, 0, 0, 101, 23, 23, 84, 85, 0, 0, 0, 0, 0, 0, 0, 0, 86, 98, 23, 120, 85, 0, 0, 0, 0, 0, 0, 0, 86, 0, 96, 97, 23, 121, 80, 81, 0, 0, 0, 0, 0, 96, 97, 0, 0, 98, 109, 125, 83, 0, 0, 0, 0, 0, 0, 0, 0, 0, 98, 100
					
					defb 80, 81, 80, 81, 82, 80, 81, 80, 81, 83, 80, 81, 80, 81, 81, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 84, 80, 81, 0, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 85, 83, 80, 81, 0, 23, 83, 0, 23, 23, 23, 23, 23, 23, 0, 80, 81, 0, 0, 23, 0, 83, 0, 23, 23, 23, 23, 23, 23, 81, 0, 0, 23, 23, 23, 0, 83, 0, 23, 23, 23, 23, 23, 40, 122, 0, 23, 23, 23, 23, 0, 0, 70, 23, 23, 23, 23, 41, 83, 0, 23, 23, 23, 23, 23, 23, 51, 0, 23, 23, 23, 0, 0, 0, 0, 23, 23, 70, 23, 23, 52, 0, 23, 70, 23, 78, 78, 78, 109, 125, 125, 78, 78, 109, 53, 78, 78, 78, 78
					defb 84, 84, 80, 81, 80, 81, 82, 80, 81, 82, 83, 80, 81, 80, 85, 85, 83, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23, 23, 23, 23, 23, 23, 23, 23, 23, 23, 0, 23, 23, 23, 83, 0, 23, 83, 0, 23, 23, 23, 23, 80, 23, 23, 23, 83, 0, 0, 23, 0, 83, 0, 23, 23, 23, 0, 23, 23, 23, 0, 0, 23, 23, 23, 0, 0, 23, 70, 23, 23, 23, 23, 83, 0, 23, 122, 23, 23, 23, 23, 80, 81, 83, 0, 23, 23, 0, 0, 23, 83, 0, 23, 23, 23, 0, 0, 0, 0, 23, 82, 0, 23, 23, 0, 0, 23, 23, 23, 23, 23, 23, 52, 78, 78, 78, 109, 125, 125, 125, 125, 125, 125, 125, 125, 125, 53
					defb 81, 83, 84, 0, 0, 0, 0, 86, 0, 0, 0, 0, 99, 101, 80, 81, 85, 0, 0, 0, 0, 96, 97, 0, 0, 0, 100, 98, 23, 84, 82, 0, 0, 0, 0, 0, 0, 0, 0, 98, 101, 100, 81, 85, 84, 0, 0, 0, 0, 0, 0, 0, 0, 0, 100, 101, 23, 83, 85, 0, 0, 0, 0, 0, 0, 0, 98, 86, 101, 99, 23, 23, 23, 0, 0, 0, 0, 98, 0, 0, 0, 96, 97, 99, 23, 23, 84, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23, 120, 85, 0, 0, 0, 0, 0, 0, 100, 0, 0, 0, 0, 51, 121, 80, 81, 86, 0, 0, 0, 0, 101, 0, 0, 86, 0, 78, 78, 78, 78, 78, 78, 78, 78, 78, 78, 78, 78, 78, 78
					
					defb 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 100, 0, 0, 0, 0, 0, 0, 86, 0, 0, 0, 0, 86, 0, 101, 0, 0, 86, 0, 0, 78, 78, 78, 78, 78, 78, 78, 78, 78, 78, 78, 78, 78, 78
End Asm

' Buffer holding the real attributes on screen,
' in case you draw an unlit screen and want to lit it
' afterwards. Space for 14x10 tiles (140 bytes)

attrsBuffer:
Asm
	attrs_buffer:	defb	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
					defb	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
					defb	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
					defb	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
					defb	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
					defb	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
					defb	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
					defb	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
					defb	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
					defb	0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
End Asm

' Data pool to interchange values BASIC <-> ASM

dataPool:
Asm
	ds_pant:		defb	0
	ds_lit:			defb	0
	ds_x:			defb 	0
	ds_y:			defb	0
	ds_xx:			defb	0
	ds_yy:			defb	0
	ds_ink_pt:		defw	0
	ds_what_tile:	defb	0
End Asm


Asm
	; Find me debugging:
	
	defb 1,2,3,1,2,3
	draw_screen:	; init pointer
	
					ld	hl, attrs_buffer
					ld	(ds_ink_pt), hl
	
					; First we find the screen address inside map_data
					; pos = map_data + n_pant * 140
					; pos = map_data + (n_pant * 128 + n_pant * 8 + n_pant * 4)
	
					ld	a, (ds_pant)
					ld	h, 0
					ld	l, a
					
					add	hl, hl			; hl = n_pant * 2
					add	hl, hl			; hl = n_pant * 4
					push hl				; stack = n_pant * 4
					add	hl, hl			; hl = n_pant * 8
					pop	de				; de = n_pant * 4
					
					add	hl, de			; hl = n_pant * 4 + n_pant * 8
					ex	de, hl			; de = n_pant * 4 + n_pant * 8; hl = n_pant * 4
					
					add	hl, hl			; * 8
					add	hl, hl			; * 16
					add	hl, hl			; * 32
					add hl, hl			; * 64
					add	hl, hl			; * 128
					add	hl, de			; * 128 + * 8 + * 4 = * 140
								
					ex	de, hl
					ld	hl, map_data
					add	hl, de			; hl = map_data + n_pant * 140.
									
					; ds_x = 2, ds_y = 2
					
					ld	a, 2
					ld	(ds_x), a
					ld	(ds_y), a
					
					; Ten rows
					
					ld	b, 10
	ds_loop1:		push bc
	
					; Fourteen columns
	
					ld	b, 14
	ds_loop2:		push bc
	
					; ** Draw current tile @ ds_x, ds_y
					
					ld	a, (hl)			; Draw tile number a
					inc	hl				; next tile
					
					push hl
					
					ld	b, a			; Save tile number in b
					
					sra	a
					sra	a
					sra	a
					sra	a
										
					ld	hl, (ds_ink_pt)
					ld 	(hl), a
					inc hl
					ld	(ds_ink_pt), hl
					
					cp	0
					jr z, nodraw		; if tile = 0, do nothing
										
					; We have to draw four characters
					
					;  32 + 4 * (a and 15) | 33 + 4 * (a and 15)
					;  -----------------------------------------
					;  34 + 4 * (a and 15) | 35 + 4 * (a and 15)
					
					; Using INK i = a / 16
					
					; Make hl point the actual char data
					; Make de point the correct screen address
					
					ld	a, b
					and	15
					
					; Now a contains the normalized, module 16 tile number
					; calculate 
					
					add	a, a			; a = a * 2
					add	a, a			; a = a * 4
					
					add	a, 32			; 32 + a * 4.
					
					ld	h, 0
					ld	l, a
					add	hl, hl
					add	hl, hl
					add	hl, hl			; hl = 8 * (32 + a * 4)
					ld	de, map_charset
					add	hl, de			; hl = map_charset + 8 * (32 + a * 4)
					
					; char1
					
					; We are goint to use ds_xx, ds_yy to point to the current char
					
					ld	a, (ds_x)
					ld	(ds_xx), a
					ld	a, (ds_y)
					ld	(ds_yy), a
					
					; CHAR 1
					
					; We calculate the address of char (ds_xx, ds_yy) into DE:
					
					call ds_calc_scr_address
						
					; Draw first char
									
					call ds_docopy
					
					; Move to (ds_xx + 1, ds_yy)
					push hl
					ld	hl, ds_xx
					inc	(hl)
					pop hl
					
					; CHAR 2
					
					call ds_calc_scr_address
					call ds_docopy
					
					; Move to (ds_xx, ds_yy + 1)
					push hl
					ld	hl, ds_xx
					dec	(hl)			; ds_xx = ds_xx + 1
					inc	hl				; hl -> ds_yy
					inc	(hl)			; ds_yy = ds_yy + 1
					pop hl
					
					; CHAR 3
					
					call ds_calc_scr_address
					call ds_docopy
					
					; Move to (ds_xx + 1, ds_yy + 1)
					push hl
					ld	hl, ds_xx
					inc	(hl)
					pop hl
					
					; CHAR 4
					
					call ds_calc_scr_address
					call ds_docopy
					
					; Done
					
	nodraw:			pop hl
					
					; ** OK
	
					push hl
					ld	hl, ds_x
					inc	(hl)
					inc	(hl)
					pop	hl
					
					pop bc
					djnz ds_loop2
					
					; ds_x = 2; ds_y = ds_y + 2.
					
					ld	a, 2
					ld 	(ds_x), a
					ld	a, (ds_y)
					inc	a
					inc	a
					ld	(ds_y), a
	
					pop bc
					djnz ds_loop1
					
					ret
	
	void_screen: 	; Clears the screen area to INK 0s.
	
					ld	de, 22594
					ld	b, 20
					xor	a
	void_screen_b1:	push bc
					ld	b, 28
	void_screen_b2: ld	(de), a
					inc	de
					djnz void_screen_b2
					pop bc
					inc de
					inc de
					inc de
					inc de
					djnz void_screen_b1
					
					ret

	defb			1,2,3,4,5,6,7,8
					
	skip_this:		; Just does ds_xx = ds_xx + 2
					push hl
					ld	hl, ds_xx
					inc	(hl)			; ds_xx = ds_xx + 1
					inc (hl) 
					pop	hl
					jr	lit_screen_nxt
	
	lit_screen:		; this lits the screen reading upon attrs_buffer
	
					; ds_x = 2, ds_y = 2
					
					ld	a, 2
					ld	(ds_xx), a
					ld	(ds_yy), a
					
					ld	hl, attrs_buffer
					
					ld	b, 10
					
	lit_screen_b1:	push bc
					ld 	b, 14	
					
	lit_screen_b2:	push bc
	
					; read attr
					ld	a, (hl)
					;cp	0
					;jr z, skip_this
					ld	b, a
					inc	hl
	
					; first char
					call ds_getattraddress
					ld	a, b
					cp	0
					jr	z, lsn0
					ld	(de), a
		lsn0:		; Move to (ds_xx + 1, ds_yy)
					push hl
					ld	hl, ds_xx
					inc	(hl)
					pop hl
					
					; second char
					call ds_getattraddress
					ld 	a, b
					cp	0
					jr	z, lsn1
					ld	(de), a
		lsn1:		; Move to (ds_xx, ds_yy + 1)
					push hl
					ld	hl, ds_xx
					dec	(hl)			; ds_xx = ds_xx - 1
					inc	hl				; hl -> ds_yy
					inc	(hl)			; ds_yy = ds_yy + 1
					pop hl
					
					; third char
					call ds_getattraddress
					ld	a, b
					cp	0
					jr	z, lsn2
					ld	(de), a
		lsn2:		; Move to (ds_xx + 1, ds_yy + 1)
					push hl
					ld	hl, ds_xx
					inc	(hl)
					pop hl
					
					; fourth char
					call ds_getattraddress
					ld	a, b
					cp	0
					jr	z, lsn3
					ld	(de), a
		lsn3:		; Move to next (ds_xx + 2, ds_yy)
					push hl
					ld	hl, ds_xx
					inc	(hl)			; ds_xx = ds_xx + 1
					inc	hl				; hl -> ds_yy
					dec	(hl)			; ds_yy = ds_yy - 1
					pop hl
	
	lit_screen_nxt:	pop bc
					djnz lit_screen_b2
					
					; Move to next row
					
					; ds_xx = 2; ds_yy = ds_yy + 2.
					
					ld	a, 2
					ld 	(ds_xx), a
					ld	a, (ds_yy)
					inc	a
					inc	a
					ld	(ds_yy), a
					
					pop	bc
					djnz lit_screen_b1	
					
					ret
					
	ds_getattraddress:

					ld		a, 	(ds_yy)		; Cogemos y
					rrca
					rrca
					rrca					; La multiplicamos por 32
					ld		e,	a			; nos lo guardamos en e
					and		3				; ponemos una mascarita 00000011
					add		a,	88			; 88 * 256 = 22528, aqu� empieza el tema
					ld		d,	a			; Hecho el bite superior.
					ld		a,	e			; Nos volvemos a traer y * 32
					and		224				; Mascarita 11100000
					ld		e,	a			; Lo volvemos a poner en e
					ld		a,	(ds_xx)		; Cogemos x
					add		a, 	e			; Le sumamos lo que ten�amos antes.
					ld		e,	a			; Listo. Ya tenemos en DE la direcci�n.
					
					ret
					
	ds_calc_scr_address:
		
					ld	a, (ds_xx)
					and	31
					ld	e,	a
					ld	a,	(ds_yy)
					ld	b,	a
					and	24
					add	a,	64
					ld	d, 	a
					ld	a,	b
					and	7
					rrca
					rrca
					rrca
					or	e
					ld	e, 	a
					
					ret		
			
	ds_docopy:		ld	a,	(hl)
					ld	(de), a
					inc	hl
					inc	d
					ld	a,	(hl)
					ld	(de), a
					inc	hl
					inc	d
					ld	a,	(hl)
					ld	(de), a
					inc	hl
					inc	d
					ld	a,	(hl)
					ld	(de), a
					inc	hl
					inc	d
					ld	a,	(hl)
					ld	(de), a
					inc	hl
					inc	d
					ld	a,	(hl)
					ld	(de), a
					inc	hl
					inc	d
					ld	a,	(hl)
					ld	(de), a
					inc	hl
					inc	d
					ld	a,	(hl)
					ld	(de), a
					inc	hl
					
					ret
					
	draw_tile:		ld	a, (ds_what_tile)
	
					; Now a contains the normalized, module 16 tile number
					; calculate 
					
					add	a, a			; a = a * 2
					add	a, a			; a = a * 4
					
					add	a, 32			; 32 + a * 4.
					
					ld	h, 0
					ld	l, a
					add	hl, hl
					add	hl, hl
					add	hl, hl			; hl = 8 * (32 + a * 4)
					ld	de, map_charset
					add	hl, de			; hl = map_charset + 8 * (32 + a * 4)
					
					; char1
					
					; We are goint to use ds_xx, ds_yy to point to the current char
					
					ld	a, (ds_x)
					ld	(ds_xx), a
					ld	a, (ds_y)
					ld	(ds_yy), a
					
					; CHAR 1
					
					; We calculate the address of char (ds_xx, ds_yy) into DE:
					
					call ds_calc_scr_address
						
					; Draw first char
									
					call ds_docopy
					
					; Move to (ds_xx + 1, ds_yy)
					push hl
					ld	hl, ds_xx
					inc	(hl)
					pop hl
					
					; CHAR 2
					
					call ds_calc_scr_address
					call ds_docopy
					
					; Move to (ds_xx, ds_yy + 1)
					push hl
					ld	hl, ds_xx
					dec	(hl)			; ds_xx = ds_xx + 1
					inc	hl				; hl -> ds_yy
					inc	(hl)			; ds_yy = ds_yy + 1
					pop hl
					
					; CHAR 3
					
					call ds_calc_scr_address
					call ds_docopy
					
					; Move to (ds_xx + 1, ds_yy + 1)
					push hl
					ld	hl, ds_xx
					inc	(hl)
					pop hl
					
					; CHAR 4
					
					call ds_calc_scr_address
					call ds_docopy
					
					; Done
					
					ret
End Asm
End Sub