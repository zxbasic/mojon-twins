'' pantallas.bas
'' 
'' Contains the vector screens
'' and code to display them
''
'' This needs suvleir loaded @63000

Sub vectorScreensContainer ()
p000p0:
asm
	incbin "..\bin\000-P0.bin"
end asm
p001p1:
asm
	incbin "..\bin\001-P1.bin"
end asm
p001p2:
asm
	incbin "..\bin\001-P2.bin"
end asm
p001p3:
asm
	incbin "..\bin\001-P3.bin"
end asm
p001p4:
asm
	incbin "..\bin\001-P4.bin"
end asm
p002p1:
asm
	incbin "..\bin\002-P1.bin"
end asm
p002p2:
asm
	incbin "..\bin\002-P2.bin"
end asm
p002p3:
asm
	incbin "..\bin\002-P3.bin"
end asm
p002p4:
asm
	incbin "..\bin\002-P4.bin"
end asm
p002p5:
asm
	incbin "..\bin\002-P5.bin"
end asm
scrolly:
asm
	incbin "..\bin\scrolly.bin"
end asm
dummy:
scrollablesky:
asm
	incbin "..\bin\c-scrollablesky.bin"
end asm
End Sub

Sub displayScreen (n As uByte) 
	Cls
	If n = 0 Then gpParseBinary (@p001p1): End If
	If n = 1 Then gpParseBinary (@p001p2): End If
	If n = 2 Then gpParseBinary (@p001p3): End If
	If n = 3 Then gpParseBinary (@p001p4): End If
	If n = 4 Then gpParseBinary (@p002p1): End If
	If n = 5 Then gpParseBinary (@p002p2): End If
	If n = 6 Then gpParseBinary (@p002p3): End If
	If n = 7 Then gpParseBinary (@p002p4): End If
	If n = 8 Then gpParseBinary (@p002p5): End If
	If n = 99 Then gpParseBinary (@p000p0): End If
End Sub

