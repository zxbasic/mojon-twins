Lhikoj
======

This game was going to be a remake of something I made circa 1991/1992 but lost completely. Originally, it was (as everything) uncompiled Sinclair BASIC which used PLOT & DRAW to create vector graphics and a nice flood fill routine I nicked from somewhere (can't remember). The game involved Phantomas, of course, and had a weird setting.

Back in 2012 I decided to try and recreate what I remembered about the original. I used ZX Basic with my own Fourspriter 2.1 and SUVLEIR 3.0 libraries. Fourspriter is modified to use XOR and no attributes.

The setting is deliberately strange. The original idea was that it was a dream inside of a dream. The main protagonist discovered what was all about as the game progressed.

What makes this stuff interesting
---------------------------------

- Modified fourspriter is faster and I've always found that XOR sprites look old-school and nice. That's why everybody thinks I'm a nerd.
- Using vector graphics for background in a sprite-based action game is pretty novel for the ZX. 
- The aPPack decompressor (aplib.bas) which may come up handy!
- It contains the word OBONABILOBOS. 
- There are tons of little assembly routines which may come handy elsewhere.

I will probably never finish this in this form, so feel free to expand.

What's in?
----------

- ./bin/ folder contains binaries which are included from the code. Mostly vector images and a couple of aplib-compressed bitmaps.
- ./dev/ folder contains da codez.
- ./gfx/ folder contains the graphics in png format. Nowadays I use flashy custom converters. Back in the day I generated C files with SevenuP and converted them to the ZX Basic syntax using my text editor and regular expresions for find & replace.
