Big Head
========

This game was created around 2010 with a really early version of ZX BASIC. It entered the CSSCGC that year and "lost", meaning that it was the "best" game, so the Mojon Twins had to host the compo in 2011.

This is a pretty simple game I made for fun during lunch breaks adding whatever I felt like adding.

Here's the original readme file:

```
This one is called "He had such a big head that if he were a cat he would 
have to toss the mice from under the bed with a brow". The story is as 
follows:

"Manolito had such a big head that he just didn't want to live anymore. He 
was about to suicide when her girlfriend suggested her to visit some african
tribemen who could actually reduce his head. He decided to go through all
the perils to achieve his goal: be normal."

The game works with "O", "P" to move and "SPACE" to jump. Note that 
Manolito's big head is the biggest hazard in the game: if you get trapped 
your only way to scape is committing suicide, pressing "S".

The game is insanely hard and somewhat unplayable, like most Spanish games 
in the 80s. True to the spirit. That's why I've added some passwords which 
let you start your game after most difficult screens.

I doubt anybody will play right to the end, so here are the passwords:

FPOL gets you to screen 12
ITYC gets you to screen 17
FORE gets you to screen 26
SERP gets you to screen 30
TESA gets you to screen 32
FINA gets you to screen 38.

If you really are a cheater, GRRR gets you directly to the ending so you can 
have a laugh.

This game has been compiled with Boriel's ZX Basic. Fully commented source
code included.
```

Here are some interesting comments found in the sources you may need to know:

```
Compiles using Boriel's ZX Basic v. 1.2.6 r 1564

just type this in a command prompt from where this source is:

$ zxb -O3 -T -B -a cabezooon.bas

Some disclaimers: 

I use random screen generators quite often. Those rely on a properly
placed Randomize which makes those random generated patters always the same.
The game is playable with the actual random number generator code included
in the compiler runtime. If this changes, patterns won't be the same.

If you want to build this game from this source code, and you get some 
unplayable situations, the random number generator may have changed. In
such case, build this using ZX Basic v. 1.2.6.
```

